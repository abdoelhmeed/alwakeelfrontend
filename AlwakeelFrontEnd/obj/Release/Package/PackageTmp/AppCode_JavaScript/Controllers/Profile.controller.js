﻿$(document).ready(function () {
    if (typeof $.cookie('UserToken') !== 'undefined') {
        var data = JSON.parse($.cookie("UserToken"));
        var token = data.access_token;
        var Name = data.FullName;
        var Contact = data.contact;
        $("#UserName").text(Name);
        $("#Contact").text(Contact);
        IntDataTable("#AdsList-data");
        GetProducts();
        GetRantCars();
        CategoriesFilter();
    }
    function GetProducts() {
        GetAuth("GET", Router.CustomerProfile.GetMyProducts, token, null, "GetMyProducts");
    }

    function GetRantCars() {
        GetAuth("GET", Router.CustomerProfile.GetMyRentCars, token, null, "GetRantCars");
    }

    async function CategoriesFilter() {
        Get("GET", Router.Shared.Categories, null, "CategoriesFilter");
    }
    $(document).on('change', '#RantCatDrop', function () {
        var id = $("#RantCatDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Brand, { CatId: id }, 'BrandFilter');
        }
        else {
            $("#RantPrandDrop").html(`<option value=''>${Resource.Sheard.SelectCat}</option>`);
            //msgError("select section first");
        }
    });
    $(document).on('change', '#RantPrandDrop', function () {
        var id = $("#RantPrandDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Class, { BrandId: id }, 'ClassFilter');
        }
        else {
            $("#RantClassDrop").html("<option value=''>Select</option>");
            //msgError("select section first");
        }
    });
    $(document).on('change', '#RantClassDrop', function () {
        var id = $("#RantPrandDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Model, { CarClassId: id }, 'ModelFilter');
        }
        else {
            $("#RantModelDrop").html("<option value=''>Select</option>");
            //msgError("select section first");
        }
    });
    //crop config
    $image_crop = $("#image_viewer").croppie({
        enbleExif: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400
        }
    });

    //crop uplode file
    $('#ImgFile').change('change', function () {
        const file = this.files[0];
        const fileType = file['type'];
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        if (validImageTypes.includes(fileType)) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            };
            reader.readAsDataURL(this.files[0]);
        }
        else {
            alert("Uplode Image");
        }
    });

    //submit data
    $("#NewAds-form").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
                function (t) {
                    if (t.value) {
                        $("#CustomPreloader").show();
                        var data = {
                            "FullName": $("#name").val(),
                            "Email": $("#email").val() === "" ? null : $("#email").val(),
                            "PhoneNumber": $("#phone").val() === "" ? null : $("#phone").val(),
                            "LoginType": $("#LoginType").val()
                        };
                        POST('POST', Router.Account.Register, data, 'register');
                    }
                    else {
                        Cancelled();
                    }
                });
    });

    $("#NewRantCar-form").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
            function (t) {
                    if (t.value) {
                        $image_crop.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(
                            function (response) {
                                $("#CustomPreloader").show();
                                let img_url;
                                if ($('#ImgFile').val() === "") {
                                    img_url = "";
                                } else {
                                    img_url = response;
                                }
                                var data = {
                                    "CatId": $("#RantCatDrop").val(),
                                    "BrandId": $("#RantPrandDrop").val(),
                                    "CarClassId": $("#RantClassDrop").val(),
                                    "ModelId": $("#RantModelDrop").val(),
                                    Image: img_url
                                };
                                POSTAuth('POST', Router.CustomerProfile.POSTRentsCarAds, token, data, 'ResponceFromServer');
                            });
                    }
                    else {
                        Cancelled();
                    }
                });
    });
});