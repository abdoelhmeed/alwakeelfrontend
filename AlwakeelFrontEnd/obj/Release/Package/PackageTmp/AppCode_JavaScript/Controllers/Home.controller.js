﻿$(document).ready(function () {
    Slider();
    MiddleAds();
    OurPartners();

    var CarsAgentLoader = true;
    var DealDayLoader = true;
    var FeatureLoader = true;
    var CertificatesdLoader = true;
    var AccessoriesLoader = true;
    var BlogsLoaded = true;
    var VideosLoader = true;
    var BrandsLoader = true;

    $(window).scroll(function () {
        if ($(window).scrollTop() >= $('.AgentContainer').offset().top - 250) {
            if (CarsAgentLoader) {
                SearchBy(1, "#Cars-agent-carousel");
                CarsAgentLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.DealsTheDayAgentContainer').offset().top - 250) {
            if (DealDayLoader) {
                DealOfTheDay();
                DealDayLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.FeatureContainer').offset().top - 250) {
            if (FeatureLoader) {
                Featured(1, "#Cars-Featured-carousel");
                FeatureLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.CertifiedContainer').offset().top - 250) {
            if (CertificatesdLoader) {
                Certificatesd();
                CertificatesdLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.AccessoriesContainer').offset().top - 250) {
            if (AccessoriesLoader) {
                Accessories(4, "#Accessories-data");
                AccessoriesLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.BlogsContainer').offset().top - 250) {
            if (BlogsLoaded) {
                Blogs();
                BlogsLoaded = false;
            }
        }
        if ($(window).scrollTop() >= $('.VideosContainer').offset().top - 350) {
            if (VideosLoader) {
                YouTubeVideos();
               VideosLoader = false;
            }
        }
        if ($(window).scrollTop() >= $('.BrandsContainer').offset().top - 350) {
            if (BrandsLoader) {
                GetBrand();
                FooterAds();
                BrandsLoader = false;
            }
        }
    });
    

    async function Slider() {
        setTimeout(function () {
            Get("GET", Router.Home.Slider, null, "Slider");
        }, 100);
    }

    async function MiddleAds() {
        setTimeout(function () {
            Get("GET", Router.Home.GetMiddleAds, null, "GetMiddleAds");
        }, 100);
    }

    // Begin SearchBy fun
    async function SearchBy(id, selector) {
        $("#SearchByLoder").html(`<img class="full_screen" src="/Assets/img/Scal/mainCompany.gif" />`);
        $("#SearchByCont").hide();
        setTimeout(function () {
            owlInitialized(selector);
            Get("GET", Router.Home.GetCompanies, { CatId: id }, "GetCompanies", selector);
        }, 100);
    }

    $(document).on("click", ".Motorcycle-agent", function () {
        SearchBy(3, "#Motorcycle-agent-carousel");
    });

    $(document).on("click", ".Trucks-agent", function () {
        SearchBy(2, "#Trucks-agent-carousel");
    });
    //End SearchBy fun

    async function OurPartners() {
        setTimeout(function () {
            //Get("GET", Router.Home.OurPartners, null, "OurPartners");
            HtmlOurPartners(SiteResource.OurPartners);
        }, 100);
    }

    async function DealOfTheDay() {
        $("#DealLoder").html(`<img class="full_screen" src="/Assets/img/Scal/DealLoder.gif" />`);
        $("#DealCont").hide();
        setTimeout(function () {
            Get("GET", Router.Home.GetDealOfday, null, "GetDealOfday");
        }, 100);
    }

    // Begin Featured fun
    async function Featured(id, selector) {
        $("#FeaturedLoder").html(`<img class="full_screen" src="/Assets/img/Scal/FeaturedLoder.gif" />`);
        $("#FeaturedCont").hide();
        setTimeout(function () {
            owlInitialized(selector);
            Get("GET", Router.Home.GetFeatured, { CatId: id }, "GetFeatured", selector);
        }, 100);
    }

    $(document).on("click", ".Motorcycle-Featured", function () {
        Featured(3, "#Motorcycle-Featured-carousel");
    });

    $(document).on("click", ".Trucks-Featured", function () {
        Featured(2, "#Trucks-Featured-carousel");
    });
    // End Featured fun

    async function Certificatesd() {
        $("#CertifiedLoder").html(`<img class="full_screen" src="/Assets/img/Scal/CertifiedLoder.gif" />`);
        $("#CertifiedCont").hide();
        setTimeout(function () {
            Get("GET", Router.Home.GetCertificatesd, null, "GetCertificatesd");
        }, 100);
    }

    async function Accessories(id, selector) {
        $("#AccessoriesLoder").html(`<img class="full_screen" src="/Assets/img/Scal/FeaturedLoder.gif" />`);
        $("#AccessoriesCont").hide();
        owlInitialized(selector);
        setTimeout(function () {
            Get("GET", Router.Home.GetAccessory, { CatId: id }, "Accessory", selector);
        }, 100);
    }

    $(document).on("click", ".Auto-Parts", function () {
        Accessories(5, "#AutoParts-data");
    });

    async function Blogs() {
        $("#BlogsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        $("#BlogsCont").hide();
        setTimeout(function () {
            Get("GET", Router.Home.GetBlog, null, "Blogs");
        }, 100);
    }

    async function YouTubeVideos() {
        $("#VideosLoder").html(`<img class="full_screen" src="/Assets/img/Scal/VideosLoder.gif" />`);
        $("#VideosCont").hide();
        setTimeout(function () {
            Get("GET", Router.Home.YouTubeVideos, null, "YouTubeVideos");
        }, 100);
    }

    async function GetBrand() {
        $("#HotBrandsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/HotBrandsLoder.gif" />`);
        $("#HotBrandsCont").hide();
        setTimeout(function () {
            Get("GET", Router.Home.GetBrand, null, "GetBrand");
        }, 100);
    }

    async function FooterAds() {
        //$("#BlogsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        //$("#AboutBox").hide();
        setTimeout(function () {
            Get("GET", Router.Home.GetFooterAds, null, "GetFooterAds");
        }, 100);
    }

    
    $(document).on("click", ".LikeBtn", function () {
        var Id = $(this).attr("id");
        Get('GET', Router.Home.GetSetlove, { Id: Id }, 'Like', Id);
    });
});