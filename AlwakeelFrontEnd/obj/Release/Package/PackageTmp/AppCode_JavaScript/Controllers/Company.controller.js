﻿$(document).ready(function () {
    ListOfCompanies();

    async function ListOfCompanies() {
        var hashes = window.location.href.slice(window.location.href.indexOf('/') + 1).split('/');
        var TypeId = hashes[4];

        setTimeout(function () {
            $("#PartnersCompaniesLoderList").show();
            $("#PartnersCompaniesLoderList").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#PartnersCompanies-data").hide();
            var url = "/Companies/List/" + TypeId;
            $("#Sub-Title").attr("href" , url);
            if (TypeId === CompanesType.AgentOil) {
                Get("GET", Router.Companies.GetAgentOil, null, "GetPartCompanies");
            }
            else if (TypeId === CompanesType.AgentBattery) {
                Get("GET", Router.Companies.GetAgentBattery, null, "GetPartCompanies");
            }
            else if (TypeId === CompanesType.AgentTires) {
                Get("GET", Router.Companies.GetAgentTires, null, "GetPartCompanies");
            }
            else if (TypeId === CompanesType.Rental) {
                Get("GET", Router.Companies.GetAgentRent, null, "GetPartCompanies");
            }
            else if (TypeId === CompanesType.Insurance) {
                Get("GET", Router.Companies.GetInsurance, null, "GetPartCompanies");
            }
        }, 100);
    }
});