﻿$(document).ready(function () {
    RentCarList();

    async function RentCarList() {
        var qsparam = new Array(10);
        var query = window.location.search.substring(1);
        var parms = query.split('&');
        for (var i = 0; i < parms.length; i++) {
            var pos = parms[i].indexOf('=');
            if (pos > 0) {
                var key = parms[i].substring(0, pos);
                var val = parms[i].substring(pos + 1);
                qsparam[i] = val;
            }
        }
        var data;
        var parm0 = qsparam[0];
        var parm1 = qsparam[1];
        var parm2 = qsparam[2]; 
        var parm3 = qsparam[3]; 
        var parm4 = qsparam[4]; 

        if (parm0 === "searchBar") {
            data = {
                "CatId": parm1, "BrandId": parm2, "CarClassId": parm3, "ModelId": parm4,
                "Skep": 0, "Take": 10
            };
            setTimeout(function () {
                Get("GET", Router.RentCar.GetRentCarSearch, data, "RentCarList");
            }, 100);
        } else {
            data = {
                "Skep": 0, "Take": 10
            };
            setTimeout(function () {
                Get("GET", Router.RentCar.GetRentCars, data, "RentCarList");
            }, 100);
        }
        
        
    }

    $(document).on('click', '.CarRequest', function (event) {
        var carId = $(this).attr('id');
        $("#CarId").val(carId);
    });

    $("#RentCar-form").submit(function (event) {
        event.preventDefault();
        $("#CustomPreloader").show();
        var CarId = $("#CarId").val();
        var name = $("#name").val();
        var phone = $("#phone").val();
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();
        if (CarId !== "") {
            var data = {
                "RId": CarId,
                "Name": name,
                "Phone": phone,
                "StartDate": startDate,
                "EndDate": endDate
            };
            POST('POST', Router.RentCar.PostRentCar, data, 'ResponceFromServer');
            $('#RentCarModal').modal('hide');
        } else {
            msgError(Resource.RentCar.SelectCar);
        }
    });
});