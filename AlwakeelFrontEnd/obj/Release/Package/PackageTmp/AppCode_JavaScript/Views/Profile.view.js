﻿function HtmlGetMyProducts(data) {
    if (data.length === 0) {
        //msgError('no data');
    }
    else {
        $('#AdsList-data').dataTable().fnDestroy();
        $('#AdsList-data').DataTable({
            "processing": true,
            "data": data,
            "columns": [
                {
                    "data": "PId", "render": function (data) {
                        let btn_action = `
                                           <a class='btn btn-primary' href='/Products/Details/${data}' /> 
                                                ${Resource.Home.ReadMore} <i class='fa fa-info-circle'></i>
                                           </a>
                                       `;
                        return btn_action;
                    }
                },
                { "data": "Info" },
                { "data": "NewEn" }
            ]
        });
    }
}

function HtmlGetRantCars(data) {
    if (data.length === 0) {
        //msgError('no data');
    }
    else {
        $('#RentCars-data').dataTable().fnDestroy();

        if (typeof $.cookie('Language') === 'undefined') {
            $('#RentCars-data').DataTable({
                "processing": true,
                "data": data,
                "columns": [
                    { "data": "CarInfoAR" },
                    {
                        "data": "ImageURl", "render": function (data) {
                            let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                            return btn_action;
                        }
                    }
                ]
            });
        } else {
            var lang = $.cookie('Language');
            if (lang === 'ar') {
                $('#RentCars-data').DataTable({
                    "processing": true,
                    "data": data,
                    "columns": [
                        { "data": "CarInfoAR" },
                        {
                            "data": "ImageURl", "render": function (data) {
                                let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                                return btn_action;
                            }
                        }
                    ]
                });
            } else {
                $('#RentCars-data').DataTable({
                    "processing": true,
                    "data": data,
                    "columns": [
                        { "data": "CarInfoEn" },
                        {
                            "data": "ImageURl", "render": function (data) {
                                let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                                return btn_action;
                            }
                        }
                    ]
                });
            }
        }
    }
}