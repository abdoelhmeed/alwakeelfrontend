﻿function HtmlGetRentCarList(data) {
    row = '';
    if (data.length > 0) {
        if (typeof $.cookie('Language') === 'undefined') {
            for (i = 0; i < data.length; i++) {
                row += `<div id="${data[i].RId}" class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="#${data[i].RId}"><img src="${data[i].ImageURl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="#${data[i].RId}">${data[i].CarInfoAR}</a></h4>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].phone}</span></div>
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].OwnerNameAR}</span></div>
                                    </div>
                                    <br />
                                    <br />
                                    <a id="${data[i].RId}" class="btn btn-primary text-white CarRequest" data-toggle="modal" data-target="#RentCarModal"><i class="fa fa-car" aria-hidden="true"></i> ${Resource.RentCar.BtnRequest}</a>
                                </div>
                            </div>
                        </div>`;
            }
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                for (i = 0; i < data.length; i++) {
                    row += `<div id="${data[i].RId}" class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="#${data[i].RId}"><img src="${data[i].ImageURl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="#${data[i].RId}">${data[i].CarInfoAR}</a></h4>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].phone}</span></div>
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].OwnerNameAR}</span></div>
                                    </div>
                                    <br />
                                    <br />
                                    <a id="${data[i].RId}" class="btn btn-primary text-white CarRequest" data-toggle="modal" data-target="#RentCarModal"><i class="fa fa-car" aria-hidden="true"></i> ${Resource.RentCar.BtnRequest}</a>
                                </div>
                            </div>
                        </div>`;
                }
            }
            else {
                for (i = 0; i < data.length; i++) {
                    row += `<div id="${data[i].RId}" class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="#${data[i].RId}"><img src="${data[i].ImageURl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="#${data[i].RId}">${data[i].CarInfoEn}</a></h4>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].phone}</span></div>
                                        <div class="col-lg-12 col-sm-12"><span><i class=""></i> ${data[i].OwnerNameEn}</span></div>
                                    </div>
                                    <br />
                                    <br />
                                    <a id="${data[i].RId}" class="btn btn-primary text-white CarRequest" data-toggle="modal" data-target="#RentCarModal"><i class="fa fa-car" aria-hidden="true"></i> ${Resource.RentCar.BtnRequest}</a>
                                </div>
                            </div>
                        </div>`;
                }
            }
        }
    } else {
        row += `<h4><a href="#">no data </a></h4>`;
    }
    $("#RentCar-data").append(row);
}