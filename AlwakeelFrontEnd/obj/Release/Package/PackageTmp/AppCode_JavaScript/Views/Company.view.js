﻿async function HtmlGetPartCompanies(data) {
    $("#PartnersCompaniesLoderList").hide();
    $("#PartnersCompanies-data").show();
    row = '';
    if (data.length > 0) {
      if (typeof $.cookie('Language') === 'undefined') {
            for (i = 0; i < data.length; i++) {
                row += `<div class="col-lg-6 col-sm-12">`;
                row += `<div class="single-latest-blog">`;
                row += `<div class="blog-img">`;
                row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img src="${data[i].BusinessLogo}" alt="blog-image"></a>`;
                row += `</div>`;

                row += `<div class="blog-desc">`;
                row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameAR}</a></h4>`;
                row += `<p class="text6">${data[i].BusinessAboutAR}</p>`;
                row += `</div>`;

                row += `</div>`;
                row += `</div>`;
            }
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                for (i = 0; i < data.length; i++) {
                    row += `<div class="col-lg-6 col-sm-12">`;
                    row += `<div class="single-latest-blog">`;
                    row += `<div class="blog-img">`;
                    row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img src="${data[i].BusinessLogo}" alt="blog-image"></a>`;
                    row += `</div>`;

                    row += `<div class="blog-desc">`;
                    row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameAR}</a></h4>`;
                    row += `<p class="text6">${data[i].BusinessAboutAR}</p>`;
                    row += `</div>`;

                    row += `</div>`;
                    row += `</div>`;
                }
            }
            else {
                for (i = 0; i < data.length; i++) {
                    row += `<div class="col-lg-6 col-sm-12">`;
                    row += `<div class="single-latest-blog">`;
                    row += `<div class="blog-img">`;
                    row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img src="${data[i].BusinessLogo}" alt="blog-image"></a>`;
                    row += `</div>`;

                    row += `<div class="blog-desc">`;
                    row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameEn}</a></h4>`;
                    row += `<p class="text6">${data[i].BusinessAboutEn}</p>`;
                    row += `</div>`;

                    row += `</div>`;
                    row += `</div>`;
                }
            }
        }
    } else {
        row += `<h4><a href="#">no data </a></h4>`;
    }
    $("#PartnersCompanies-data").append(row);
}
async function HtmlGetInsuranceById(data) {
    if (typeof $.cookie('Language') === 'undefined') {
        var loadDataAr = {
            "BusinessId": data.BusinessId,
            "BusinessName": data.BusinessNameAR,
            "BusinessLogo": data.BusinessLogo,
            "BusinessAbout": data.BusinessAboutAR,
            "BusinessPanelImage": data.BusinessPanelImage,
            "BusinessType": data.BusinessType,
            "ProductLoad": data.Products,
            "ContactLoad": data.Contact,
            "companyWithBrand": data.companyWithBrand
        };
        HtmlViewInsuranceById(loadDataAr);
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            var loadDataAr2 = {
                "BusinessId": data.BusinessId,
                "BusinessName": data.BusinessNameAR,
                "BusinessLogo": data.BusinessLogo,
                "BusinessAbout": data.BusinessAboutAR,
                "BusinessPanelImage": data.BusinessPanelImage,
                "BusinessType": data.BusinessType,
                "ProductLoad": data.Products,
                "ContactLoad": data.Contact,
                "companyWithBrand": data.companyWithBrand
            };
            HtmlViewInsuranceById(loadDataAr2);
        } else {
            var loadData = {
                "BusinessId": data.BusinessId,
                "BusinessName": data.BusinessNameEn,
                "BusinessLogo": data.BusinessLogo,
                "BusinessAbout": data.BusinessAboutEn,
                "BusinessPanelImage": data.BusinessPanelImage,
                "BusinessType": data.BusinessType,
                "ProductLoad": data.Products,
                "ContactLoad": data.Contact,
                "companyWithBrand": data.companyWithBrand
            };
            HtmlViewInsuranceById(loadData);
        }
    }
}
async function HtmlViewInsuranceById(data) {
    if (data.BusinessType === "Insurance " || data.BusinessType === "Bank") {
        $("#LQuantity").hide();
        $("#Quantity").hide();
        
    } else {
        $("#LQuantity").show();
        $("#Quantity").show();
    }
    $("#Comp-Name").text(data.BusinessName);
    row = ``;
    row += `<div  class="row">
            <div class="col-lg-3 order-2 order-lg-1">
                <aside class="card-box">
                    <div class="col-img mb-30">
                        <a href=""><img src="${data.BusinessLogo}" alt="slider-banner"></a>
                    </div>
                    <div class="single-sidebar mb-30">
                        <h3 class="sidebar-Name">${data.BusinessName}</h3></div>
                    <div class="tags">
                        <h3 class="sidebar-title">${Resource.Company.Contact_Social}</h3>
                        <div class="sidbar-style">
                            <ul class="tag-list">`;
    for (var i1 = 0; i1 < data.ContactLoad.length; i1++) {
        if (data.ContactLoad[i1].Type === "Phone Number") {
            row += ` <li><a href="tel:${data.ContactLoad[i1].contact}">${data.ContactLoad[i1].contact}<i class="${data.ContactLoad[i1].Icons}"></i></a></li>`;
        } else {
            row += ` <li><a href="${data.ContactLoad[i1].contact}"  target="_blank">${data.ContactLoad[i1].Type}<i class="${data.ContactLoad[i1].Icons}"></i></a></li>`;
        }
    }
    row += `</ul></div></div></aside></div>`;

    //row 2
    row += `<div class="col-lg-9 order-1 order-lg-2">
                  <div class="single-sidebar-desc mb-all-40">
                      <div class="sidebar-img">
                          <div id="slider-box" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img class="slide-img" src="${data.BusinessPanelImage[0]}" alt="slider" title="#slider-direction-3" /></div>`;
                                    for (var i = 1; i < data.BusinessPanelImage.length; i++) {
                                        row += `<div class="carousel-item"><img class="slide-img" src="${data.BusinessPanelImage[i]}" alt="slider" title="#slider-direction-4" /></div>`;
                                    }
                                    row += `</div>
                                    <a class="carousel-control-prev" href="#slider-box" role="button" data-slide="prev" >
                                    <span class="fa fa-arrow-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
                                    <a class="carousel-control-next" href="#slider-box" role="button" data-slide="next">
                                    <span class="fa fa-arrow-right" aria-hidden="true" ></span><span class="sr-only">Next</span></a>`;
                        row += `</div>
                            </div>
                            <div class="sidebar-desc mb-50">
                              <p>${data.BusinessAbout}</p>
                            </div>
                        </div>
                    </div>
                </div>`;
    row += `</div></div>`;
    if (data.BusinessType === "Company") {
        row += `<div class="main-blog"> <h2 class="Location-title"> ${Resource.Company.Brand} </h2>`;
    }
    $("#ComBody-data").append(row);

    if (data.BusinessType === "Company") {
        for (var c = 0; c < data.companyWithBrand.length; c++) {
            row = '';
            row += `<div class="single-product comp-Brand">
                            <div class="pro-img">
                                <a href="#" id="${data.companyWithBrand[c].BrandId}" class="GetProdectBrand" >
                                    <img class="primary-img" src="${data.companyWithBrand[c].BrandsLogo}" alt="single-product">
                                    <img class="secondary-img" src="${data.companyWithBrand[c].BrandsLogo}" alt="single-product">
                                </a>
                            </div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="">${data.companyWithBrand[c].BrandNameAr}</a></h4>
                                </div>
                            </div>
                        </div>`;
            $("#companyWithBrand").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    }
    if (data.ProductLoad.length === 0) {
        $("#companyWithProducts").html("<h4 class='text-center'>No data....</h4>");
    } else {
    for (var p = 0; p < data.ProductLoad.length; p++) {
            row = '';
            row += `<div class="single-product comp-Brand">
                            <div class="pro-img">
                                <a href="">
                                    <img class="primary-img" src="${data.ProductLoad[p].PimageUrl}" alt="single-product">
                                    <img class="secondary-img" src="${data.ProductLoad[p].PimageUrl}" alt="single-product">
                                </a>
                            </div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="">${data.ProductLoad[p].PNameAR}</a></h4>
                                     <p><span class="price">${data.ProductLoad[p].Price === "0" ? data.ProductLoad[p].Price : ""} ${Resource.Home.SDG}</span></p>
                                </div>
                            </div>
                        </div>`;
            $("#companyWithProducts").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    }
}