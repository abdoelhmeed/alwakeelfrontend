﻿
async function HtmlHeaderAds(data) {
    $("#pop-banner").attr("src",data[0].Image);
}
async function HtmlCategoriesFilter(data) {
    $("#CatDrop").html(`<option value=''>${Resource.Sheard.SelectCat}</option>`);
    $("#RantCatDrop").append(`<option value=''>${Resource.Sheard.SelectCat}</option>`);
    $("#CatsMenu").append("");
    
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].CatId + '">' + data[i].NameAr + '</option>';
            row1 += `<li class=""><a href="/Products/List?filter=searchBar&CatId=${data[i].CatId}">${data[i].NameAr} <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>`;
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CatId + '">' + data[i].NameAr + '</option>';
                row1 += `<li class=""><a href="/Products/List?filter=searchBar&CatId=${data[i].CatId}">${data[i].NameAr} <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>`;
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CatId + '">' + data[i].NameEn + '</option>';
                row1 += `<li class=""><a href="/Products/List?filter=searchBar&CatId=${data[i].CatId}">${data[i].NameEn} <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>`;

            }
        }
    }

    $("#CatDrop").append(row);
    $("#RantCatDrop").append(row);
    $("#CatsMenu").append(row1);
}
async function HtmlBrandFilter(data) {
    $("#PrandDrop").html("");
    $("#PrandList").html("");
    $("#RantPrandDrop").html("");
    
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].BrandId + '">' + data[i].BrandNameAr + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].BrandId + '">' + data[i].BrandNameAr + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].BrandId + '">' + data[i].BrandNameEn + '</option>';
            }
        }
    }
    $("#PrandDrop").append(row);
    $("#PrandList").append(row);
    $("#RantPrandDrop").append(row);
    
}
async function HtmlClassFilter(data) {
    $("#ClassDrop").html("");
    $("#RantClassDrop").html("");
    $("#ClassList").html("");
    
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].CarClassId + '">' + data[i].CarClassNameAr + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CarClassId + '">' + data[i].CarClassNameAr + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CarClassId + '">' + data[i].CarClassNameEn + '</option>';
            }
        }
    }
    $("#ClassDrop").append(row);
    $("#RantClassDrop").append(row);
    
    $("#ClassList").append(row);
}
async function HtmlModelFilter(data) {
    $("#ModelDrop").html("");
    $("#RantModelDrop").html("");
    
    $("#ModelList").html("");
    var row = '';
    var i = 0;
    for (i; i < data.length; i++) {
        row += '<option value="' + data[i].ModelId + '">' + data[i].ModelYears + '</option>';
    }
    $("#ModelDrop").append(row);
    $("#RantModelDrop").append(row);
    $("#ModelList").append(row);
}
async function HtmlMenuOurPartners(data) {
    $("#PartnersDrop").html("");
    $("#PartnersMenu,#PartnersMenuMobile").html("");
    row = '';
    var row1 = '';
    for (i = 0; i < data.length; i++) {
        if (typeof $.cookie('Language') === 'undefined') {
            if (i === 0) {
                row1 += `<li><a style="padding:5px !important" href="">${data[i].NameAr}</a></li>`;
            } else if (i !== 5) {
                row1 += `<li><a style="padding:5px !important" href="/Companies/List/${data[i].Id}">${data[i].NameAr}</a></li>`;
            } else {
                row1 += `<li><a style="padding:5px !important" href="/Companies/TrafficPolice/${data[i].Id}">${data[i].NameAr}</a></li>`;
            }
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                if (i === 0) {
                    row1 += `<li><a style="padding:5px !important" href="">${data[i].NameAr}</a></li>`;
                } else if (i !== 5) {
                    row1 += `<li><a style="padding:5px !important" href="/Companies/List/${data[i].Id}">${data[i].NameAr}</a></li>`;
                } else {
                    row1 += `<li><a style="padding:5px !important" href="/Companies/TrafficPolice/${data[i].Id}">${data[i].NameAr}</a></li>`;
                }
            }
            else {
                if (i === 0) {
                    row1 += `<li><a style="padding:5px !important" href="">${data[i].Name}</a></li>`;
                } else if (i !== 5) {
                    row1 += `<li><a style="padding:5px !important" href="/Companies/List/${data[i].Id}">${data[i].Name}</a></li>`;
                } else {
                    row1 += `<li><a style="padding:5px !important" href="/Companies/TrafficPolice/${data[i].Id}">${data[i].Name}</a></li>`;
                }
            }
        }
    }
    $("#PartnersMenu").append(row1);
    $("#PartnersMenuMobile").append(row1);
}

async function owlInitialized(selector) {
    for (var i = 0; i < $('.owl-stage').length; i++) {
        $(selector).trigger('remove.owl.carousel', [i])
            .trigger('refresh.owl.carousel');
    }
}

