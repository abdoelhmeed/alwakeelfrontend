﻿//---------------------------------Message Screen
function msgError(msg) {
    Swal.fire(
        {
            type: "error",
            title: Resource.Base.LabelcanTitle,
            text: msg,
            confirmButtonClass: "btn btn-confirm mt-2"
        });
}
//Success Message
function msgSuccess(msg) {
    var title;
    Swal.fire(
        {
            title: Resource.Base.LabelgodTitle,
            text: msg,
            type: "success",
            confirmButtonClass: "btn btn-confirm mt-2"
        });
}
//Cancelled Message
function Cancelled() {
    Swal.fire({ title: Resource.Base.LabelcanTitle, text: Resource.Base.LabelcanTitle, type: "error" });
}

function IntDataTable(selector) {
    $(selector).DataTable();
}