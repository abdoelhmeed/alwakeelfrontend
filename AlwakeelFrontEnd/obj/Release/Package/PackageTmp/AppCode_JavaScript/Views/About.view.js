﻿async function HtmlGetAboutAlwakeel(data) {
    $("#AboutLoder").html("");
    $("#AboutBox").show();
    $("#AboutImg").attr("src", data.BusinessPanelImage);
    if (typeof $.cookie('Language') === 'undefined') {
        $("#AboutTitle").text(data.BusinessNameAR);
        $("#AboutBody").text(data.BusinessAboutAR);
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            $("#AboutTitle").text(data.BusinessNameAR);
            $("#AboutBody").text(data.BusinessAboutAR);
        }
        else {
            $("#AboutTitle").text(data.BusinessNameEn);
            $("#AboutBody").text(data.BusinessAboutEn);
        }
    }
}