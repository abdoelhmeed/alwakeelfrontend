﻿//English Resource File
const Resource = {
    "HTTP": {
        "": "",
        "": "",
        "": "",
        "": ""
    },
    "Base": {
        "Labeltitle": "Sorry",
        "LabelgodTitle": "Good job!",
        "LabelcanTitle": "Cancelled",
        "Labeltext": "Your data is safe :)"
    },
    "Register": {
        "title": "Are you sure?",
        "text": "You won't be able to revert this!",
        "confirmButtonText": "Yes, I want to register a new account",
        "cancelButtonText": "No, cancel!"
    },
    "Contact": {
        "confirmButtonText": "Yes, I want to send the message",
        "cancelButtonText": "No, cancel!"
    },
    "Login": {
        "title": "Are you sure?",
        "text": "You won't be able to revert this!",
        "confirmButtonText": "Yes, I want to log in to my account",
        "cancelButtonText": "No, cancel!"
    },
    "Sheard": {
        "SelectCat":"Select category",
        "EnterEmail":"Enter your email"
    },
    "Home": {
        "SDG": "SDG",
        "Certified": "Certified",
        "AddToWishListBtn": "Like",
        "AddToCartBtn": "Add to Cart",
        "Featured":"Featured",
        "LatestVideos": "Latest Videos",
        "Accessories&AutoParts": "Accessories & Auto Parts",
        "ReadMore": "Read More"
    },
    "Company": {
        "Brand": "Brands",
        "SelectCompany": "Select Company first",
        "Contact_Social": "contact & Social media"
    },
    "RentCar": {
        "BtnRequest": "Vehicle rental request",
        "SelectCar": "Select the Car first"
    },
    "PorductDetails": {
        "Color": "Color :",
        "EngineCapacity": "Engine Capacity",
        "MileageKM": "Mileage KM",
        "Contacts": "Contacts",
        "Views": "Views",
        "Love": "like",
        "FeatureDetails": "Feature Details"
    }
};
