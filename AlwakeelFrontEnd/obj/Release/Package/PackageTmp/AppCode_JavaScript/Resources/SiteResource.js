﻿//English Resource File
const SiteResource = {
    "Slider": [
        "/Assets/img/slider/1.jpg",
        "/Assets/img/slider/2.jpg"
    ],
    "OurPartners": [
        {
            "Id": "0",
            "Name": "Al Baraka Bank",
            "NameAr": "بنك البركة",
            "Img": "/Assets/img/Partners/0.jpg",
            "UrlPage": "#",
            "Date": "02/05/2020"
        },
        {
            "Id": "1",
            "Name": "Agent Oil",
            "NameAr": "وكيل زيت",
            "Img": "/Assets/img/Partners/1.jpg",
            "UrlPage": "/Companies/List/",
            "Date": "02/05/2020"
        },
        {
            "Id": "2",
            "Name": "Agent Battery",
            "NameAr": "وكيل بطارية",
            "Img": "/Assets/img/Partners/2.jpg",
            "UrlPage": "/Companies/List/",
            "Date": "02/05/2020"

        },
        {
            "Id": "3",
            "Name": "Agent Tires",
            "NameAr": "وكيل اطارات",
            "Img": "/Assets/img/Partners/3.jpg",
            "UrlPage": "/Companies/List/",
            "Date": "04/05/2020"

        },
        {
            "Id": "4",
            "Name": "Rental",
            "NameAr": "تأجير",
            "Img": "/Assets/img/Partners/4.jpg",
            "UrlPage": "/Companies/List/",
            "Date": "08/05/2020"
        },
        {
            "Id": "5",
            "Name": "Traffic police",
            "NameAr": "شرطة المرور",
            "Img": "/Assets/img/Partners/5.jpg",
            "UrlPage": "/Companies/TrafficPolice/",
            "Date": "09/05/2020"
        },
        {
            "Id": "6",
            "Name": "Insurance",
            "NameAr": "تأمين",
            "Img": "/Assets/img/Partners/6.jpg",
            "UrlPage": "/Companies/List/",
            "Date": "09/05/2020"
        }
    ]
};
