﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelFrontEnd.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account / Login
        public ActionResult Login()
        {
            HttpCookie UserToken = Request.Cookies["UserToken"];
            if (UserToken == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Profile", "Account");
            }
        }
        // GET: Account / Login
        public ActionResult Verification()
        {
            HttpCookie UserToken = Request.Cookies["UserToken"];
            if (UserToken == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Profile", "Account");
            }
        }

        // GET: Account / Register
        public ActionResult Register()
        {
            HttpCookie UserToken = Request.Cookies["UserToken"];
            if (UserToken == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Profile", "Account");
            }
        }

        // GET: Account / Regular User
        public ActionResult Profile()
        {
            HttpCookie UserToken = Request.Cookies["UserToken"];
            if (UserToken != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}