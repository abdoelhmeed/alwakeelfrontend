﻿async function HtmlGetBlog(data) {
    $("#BlogsLoder").html("");
    $("#Blogs-data").show();

    row = '';
    if (data.length > 0) {
        if (typeof $.cookie('Language') === 'undefined') {
            for (i = 0; i < data.length; i++) {
                row += `<div class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="/blogs/Dtails/${data[i].blogId}">${data[i].blogTitleAr}</a></h4>
                                    <p class="text6">${data[i].blogBodyAr.replace(/<(.|\n)*?>/g, '')}</p>
                                    <a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>
                                </div>
                            </div>
                        </div>`; 
            }
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                for (i = 0; i < data.length; i++) {
                    row += `<div class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="/blogs/Dtails/${data[i].blogId}">${data[i].blogTitleAr}</a></h4>
                                    <p class="text6">${data[i].blogBodyAr.replace(/<(.|\n)*?>/g, '')}</p>
                                    <a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>
                                </div>
                            </div>
                        </div>`; 
                }
            }
            else {
                for (i = 0; i < data.length; i++) {
                    row += `<div class="col-lg-6 col-sm-12">
                            <div class="single-latest-blog">
                                <div class="blog-img">
                                    <a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a>
                                </div>
                                <div class="blog-desc">
                                    <h4><a href="/blogs/Dtails/${data[i].blogId}">${data[i].blogTitleEn}</a></h4>
                                    <p class="text6">${data[i].blogBodyEn.replace(/<(.|\n)*?>/g, '')}</p>
                                    <a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>
                                </div>
                            </div>
                        </div>`; 
                }
            }
        }
    } else {
        row += `<h4><a href="#">no data </a></h4>`;
    }
    $("#Blogs-data").append(row);
}

async function HtmlGetBlogdetails(data) {
    $("#BlogsdetailLoder").html("");
    $("#Blogsdetail-data").show();
    $("#BlogImage").attr("src", data.blogIamgeUrl);
    $("#BlogDate").text(convertdate(data.blogDate));

    if (typeof $.cookie('Language') === 'undefined') {
        $("#BlogName").text(data.blogTitleAr);
        $("#TitleBogeName").text(data.blogTitleAr);
        $("#BlogBody").html(data.blogBodyAr);
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            $("#BlogName").text(data.blogTitleAr);
            $("#TitleBogeName").text(data.blogTitleAr);
            $("#BlogBody").html(data.blogBodyAr);
        }
        else {
            $("#BlogName").text(data.blogTitleEn);
            $("#TitleBogeName").text(data.blogTitleEn);
            $("#BlogBody").html(data.blogBodyEn);
        }
    }

}