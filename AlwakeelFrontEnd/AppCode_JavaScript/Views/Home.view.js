﻿/* Home view */
async function HtmlSlider(data) {
    $("#slider-box").html("");
    row = '';
    row += `<ol class="carousel-indicators">`;
    row += `<li data-target="#slider-box" data-slide-to="0" class="active"></li>`;
    for (i = 1; i < data.length; i++) {
        row += `<li data-target="#slider-box" data-slide-to="${i}"></li>`;
    }
    row += `</ol>`;
    row += `<div class="carousel-inner">`;
    row += `<div class="carousel-item active"><img class="slide-img" src="${data[0].adsImage}" alt="slider" title="#slider-direction-3" /></div>`;
    for (j = 1; j < data.length; j++) {
        row += `<div class="carousel-item"><img class="slide-img" src="${data[j].adsImage}" alt="slider" title="#slider-direction-4" /></div>`;
    }
    row += `</div>`;
    row += `<a class="carousel-control-prev" href="#slider-box" role="button" data-slide="prev" >`;
    row += `<span class="fa fa-arrow-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a>`;
    row += `<a class="carousel-control-next" href="#slider-box" role="button" data-slide="next">`;
    row += `<span class="fa fa-arrow-right" aria-hidden="true" ></span><span class="sr-only">Next</span></a>`;
    $("#slider-box").append(row);
}

async function HtmlMiddleAds(data) {
    row = '';
    $("#adsMid-data").html("");
    for (i = 0; i < data.length; i++) {
        row += `<div class="col-img">
            <a href="${data[i].Link}">
                <img src="${data[i].Image}" alt="Ads">
            </a>
        </div>`;
    }
    $("#adsMid-data").append(row);
}

async function HtmlGetCompanies(data, selctor) {
    $("#SearchByLoder").html("");
    $("#SearchByCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            row = '';
            //1
            row += `<div class="single-product red-border" style="width: 330px;">`;

            //2
            row += `<div class="pro-img">`;
            row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img class="primary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameAR}">
                    <img class="secondary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameAR}"></a>`;
            row += `</div>`;

            //3
            row += `<div class="pro-content">`;
            row += `<div class="pro-info">`;
            row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameAR}</a></h4>`;
                    row += `<div class="box-Brand">`;
                        for (var j = 0; j < data[i].companyWithBrand.length; j++) {
                            row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[j].BrandsLogo}" /><h4> ${data[i].companyWithBrand[j].BrandNameAr} </h4></div>`;
                        }
                    row += `</div>`;
                row += `</div>`;

                row += `<div class="pro-actions">`;
                    row += `<div class="actions-primary">`;
                        row += `<div class="box-Brand">`;
                        for (var _j = 0; _j < data[i].companyWithBrand.length; _j++) {
                            row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[_j].BrandsLogo}" /><h4> ${data[i].companyWithBrand[_j].BrandNameAr} </h4></div>`;
                        }
                        row += `</div>`;
                    row += `</div>`;
                row += `</div>`;
            //1
            row += `</div>`;
            $(selctor).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    }
    else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                row = '';
                //1
                row += `<div class="single-product red-border" style="width: 330px;">`;

                //2
                row += `<div class="pro-img">`;
                row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img class="primary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameAR}">
                    <img class="secondary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameAR}"></a>`;
                row += `</div>`;

                //3
                row += `<div class="pro-content">`;
                row += `<div class="pro-info">`;
                row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameAR}</a></h4>`;
                row += `<div class="box-Brand">`;
                for (var j1 = 0; j1 < data[i].companyWithBrand.length; j1++) {
                    row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[j1].BrandsLogo}" /><h4> ${data[i].companyWithBrand[j1].BrandNameAr} </h4></div>`;
                }
                row += `</div>`;
                row += `</div>`;

                row += `<div class="pro-actions">`;
                row += `<div class="actions-primary">`;
                row += `<div class="box-Brand">`;
                for (var _j1 = 0; _j1 < data[i].companyWithBrand.length; _j1++) {
                    row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[_j1].BrandsLogo}" /><h4> ${data[i].companyWithBrand[_j1].BrandNameAr} </h4></div>`;
                }
                row += `</div>`;
                row += `</div>`;
                row += `</div>`;
                //1
                row += `</div>`;
                $(selctor).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                row = '';
                //1
                row += `<div class="single-product red-border" style="width: 330px;">`;

                //2
                row += `<div class="pro-img">`;
                row += `<a href="/Companies/Viewditels/${data[i].BusinessId}"><img class="primary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameEn}">
                    <img class="secondary-img resize-Agent" src="${data[i].BusinessLogo}" alt="${data[i].BusinessNameEn}"></a>`;
                row += `</div>`;

                //3
                row += `<div class="pro-content">`;
                row += `<div class="pro-info">`;
                row += `<h4><a href="/Companies/Viewditels/${data[i].BusinessId}">${data[i].BusinessNameEn}</a></h4>`;
                row += `<div class="box-Brand">`;
                for (var j2 = 0; j2 < data[i].companyWithBrand.length; j2++) {
                    row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[j2].BrandsLogo}" /><h4> ${data[i].companyWithBrand[j2].BrandNameEn} </h4></div>`;
                }
                row += `</div>`;
                row += `</div>`;

                row += `<div class="pro-actions">`;
                row += `<div class="actions-primary">`;
                row += `<div class="box-Brand">`;
                for (var _j2 = 0; _j2 < data[i].companyWithBrand.length; _j2++) {
                    row += `<div class="item-Brand"><img class="Brand-img" src="${data[i].companyWithBrand[_j2].BrandsLogo}" /><h4> ${data[i].companyWithBrand[_j2].BrandNameEn} </h4></div>`;
                }
                row += `</div>`;
                row += `</div>`;
                row += `</div>`;
                //1
                row += `</div>`;
                $(selctor).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlOurPartners(data) {
    $("#Partners-data").html("");
    var Group = parseInt(data.length / 3);
    var currentPage = 0;
    row = '';
    row += `<div class="banner-box"><div class="col-img box-resize"><a href="#"><img src="${data[0].Img}" alt="${data[0].NameAr}"></a></div></div>`;
    for (i = 0; i < Group; i++) {
        row += `<div class="banner-box">`;
        for (j = 1; j < 4; j++) {
            let index = parseInt(j + currentPage);
            if (typeof $.cookie('Language') === 'undefined') {
                row += `<div class="col-img"><a href="${data[index].UrlPage}${data[index].Id}"><img class="Img-resize" src="${data[index].Img}" alt="${data[index].NameAr}"></a></div>`;
            } else {
                lang = $.cookie('Language');
                if (lang === 'ar') {
                    row += `<div class="col-img"><a href="${data[index].UrlPage}${data[index].Id}"><img class="Img-resize" src="${data[index].Img} " alt="${data[index].NameAr}"></a></div>`;
                }
                else {
                    row += `<div class="col-img"><a href="${data[index].UrlPage}${data[index].Id}"><img class="Img-resize" src="${data[index].Img}" alt="${data[index].Name}"></a></div>`;
                }
            }
        }
        row += `</div>`;
        currentPage = parseInt(currentPage + 3);
    }
    $("#Partners-data").append(row);
}

async function HtmlDealOfday(data) {
    $("#DealLoder").html("");
    $("#DealCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            priceConvertO = await this.ConvertO(data[i].Price);
            row = '';
            row += `
                <div class="row">
                    <div class="col-lg-6 mb-all-40 hot-product2">
                        <div class="tab-content"> 
                            <div id="thumb${0}" class="tab-pane fade show active">
                                <a data-fancybox="images" href="/${data[i].Image[0]}"><img src="${data[i].Image[0]}" alt="product-view"></a>
                            </div>`;

            for ( var j = 1; j < data[i].Image.length; j++) {
                row += ` <div id="thumb${j}" class="tab-pane fade">
                            <a data-fancybox="images" href="${data[i].Image[j]}"><img src="${data[i].Image[j]}" alt="product-view"></a>
                         </div> `;
            }

            row += `</div>
                        <div class="product-thumbnail">
                            <div class="pro-tab-menu nav tabs-area" role="tablist">
                                <a class="active" data-toggle="tab" href="#thumb${0}"><img src="${data[i].Image[0]}" alt="product-thumbnail"></a>`;
            for (var x = 1; x < data[i].Image.length; x++) {
                    row += `<a data-toggle="tab" href="#thumb${x}"><img src="${data[i].Image[x]}" alt="product-thumbnail"></a>`;
            }
                               
            row += `</div>
                     </div>
                    </div>
                    <div class="col-lg-6 hot-product2">
                        <div class="thubnail-desc fix">
                            <h3><a href="/Products/Details/${data[i].PId}">${data[i].CatNameAR}</a></h3>
                            <div class="pro-price mtb-30">
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                            </div>
                            <p class="mb-30 pro-desc-details">${data[i].VehicleInfoAR}</p>`;
                            if (data[i].NewAR !== '') { row += `<span class="sticker-new custom-sticker-new"> ${data[i].NewAR }</span>`; }
                           row +=`<div class="row">
                                 <div class="col-lg-5">views <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                <div class="col-lg-5">Love <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                            </div>                        
                        </div>
                    </div>
                </div>`;
            $("#Deal-data").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    }
    else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);
                row = '';
                row += `
                <div class="row">
                    <div class="col-lg-6 mb-all-40 hot-product2">
                        <div class="tab-content"> 
                            <div id="thumb${0}" class="tab-pane fade show active">
                                <a data-fancybox="images" href="/${data[i].Image[0]}"><img src="${data[i].Image[0]}" alt="product-view"></a>
                            </div>`;

                for ( j = 1; j < data[i].Image.length; j++) {
                    row += `<div id="thumb${j}" class="tab-pane fade">
                                <a data-fancybox="images" href="${data[i].Image[j]}"><img src="${data[i].Image[j]}" alt="product-view"></a>
                             </div>`;
                }


                row += `</div>
                        <div class="product-thumbnail">
                            <div class="pro-tab-menu nav tabs-area" role="tablist">
                                <a class="active" data-toggle="tab" href="#thumb${0}"><img src="${data[i].Image[0]}" alt="product-thumbnail"></a>`;
                for (x = 1; x < data[i].Image.length; x++) {
                    row += `<a data-toggle="tab" href="#thumb${x}"><img src="${data[i].Image[x]}" alt="product-thumbnail"></a>`;
                }

                row += `</div>
                     </div>
                    </div>
                    <div class="col-lg-6 hot-product2">
                        <div class="thubnail-desc fix">
                            <h3><a href="/Products/Details/${data[i].PId}">${data[i].CatNameAR}</a></h3>
                            <div class="pro-price mtb-30">
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                            </div>
                            <p class="mb-30 pro-desc-details">${data[i].VehicleInfoAR}</p>`;
                if (data[i].NewAR !== '') { row += `<span class="sticker-new custom-sticker-new"> ${data[i].NewAR}</span>`; }
                            row +=`<div class="row">
                              <div class="col-lg-5">views <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                <div class="col-lg-5">Love <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                            </div>                            
</div>
                    </div>
                </div>`;
                $("#Deal-data").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);

                row = '';
                row += `
                <div class="row">
                    <div class="col-lg-6 mb-all-40 hot-product2">
                        <div class="tab-content"> 
                            <div id="thumb${0}" class="tab-pane fade show active">
                                <a data-fancybox="images" href="/${data[i].Image[0]}"><img src="${data[i].Image[0]}" alt="product-view"></a>
                            </div>`;

                for ( j = 1; j < data[i].Image.length; j++) {
                    row += ` <div id="thumb${j}" class="tab-pane fade">
                            <a data-fancybox="images" href="${data[i].Image[j]}"><img src="${data[i].Image[j]}" alt="product-view"></a>
                         </div> `;
                }


                row += `</div>
                        <div class="product-thumbnail">
                            <div class="pro-tab-menu nav tabs-area" role="tablist">
                                <a class="active" data-toggle="tab" href="#thumb${0}"><img src="${data[i].Image[0]}" alt="product-thumbnail"></a>`;
                for (x = 1; x < data[i].Image.length; x++) {
                    row += `<a data-toggle="tab" href="#thumb${x}"><img src="${data[i].Image[x]}" alt="product-thumbnail"></a>`;
                }

                row += `</div>
                     </div>
                    </div>
                    <div class="col-lg-6 hot-product2">
                        <div class="thubnail-desc fix">
                            <h3><a href="/Products/Details/${data[i].PId}">${data[i].CatNameEn}</a></h3>
                            <div class="pro-price mtb-30">
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                            </div>
                            <p class="mb-30 pro-desc-details">${data[i].VehicleInfoEn}</p>`;
                if (data[i].NewEn !== '') { row += `<span class="sticker-new custom-sticker-new"> ${ data[i].NewEn }</span >`; }
                            
                          row += `<div class="row">
                                <div class="col-lg-5">views <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                <div class="col-lg-5">Love <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                            </div>                            
                        </div>
                    </div>
                </div>`;
                $("#Deal-data").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlFeatured(data, selector) {
    $("#FeaturedLoder").html("");
    $("#FeaturedCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            priceConvertO = await this.ConvertO(data[i].Price);
            row = '';
            row += `<div class="single-product">
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                            </a>`;
                            if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
                            row += `</div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoAR}</a></h4>
                                     <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                    <div class="label-product l_sale"> ${Resource.Home.Featured} </div>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
            $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);

                row = '';
                row += `<div class="single-product">
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                                </a>`;
                            if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
                            row += `</div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoAR}</a></h4>
                                     <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                    <div class="label-product l_sale"> ${Resource.Home.Featured} </div>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);
                row = '';
                row += `<div class="single-product">
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                                </a>`;
                if (data[i].NewEn !== '') { row += `<span class="sticker-new">${data[i].NewEn}</span>`; }
                row += `</div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoEn}</a></h4>
                                     <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                    <div class="label-product l_sale"> ${Resource.Home.Featured} </div>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlCertificatesd(data) {
    $("#CertifiedLoder").html("");
    $("#CertifiedCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            priceConvertO = await this.ConvertO(data[i].Price);
            row = '';
            row += `<div class="single-product">
                        <div class="pro-img">
                            <a href="/Products/Details/${data[i].PId}">
                                <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                            </a>`;
            if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
            row += `</div>
                        <div class="pro-content">
                            <div class="pro-info">
                                <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoAR}</a></h4>
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                <div class="label-product l_sale"> ${Resource.Home.Certified} </div>
                            </div>
                            <div class="pro-actions">
                                <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                            </div>
                        </div>
                    </div>`;
            $('#Certificatesd-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);

                row = '';
                row += `<div class="single-product">
                        <div class="pro-img">
                            <a href="/Products/Details/${data[i].PId}">
                                <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                        </a>`;
                if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
                    row += `</div>
                        <div class="pro-content">
                            <div class="pro-info">
                                <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoAR}</a></h4>
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                <div class="label-product l_sale"> ${Resource.Home.Certified} </div>
                            </div>
                            <div class="pro-actions">
                                 <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                            </div>
                        </div>
                    </div>`;
                $('#Certificatesd-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].Price);

                row = '';
                row += `<div class="single-product">
                        <div class="pro-img">
                            <a href="/Products/Details/${data[i].PId}">
                                <img class="primary-img" src="${data[i].Image}" alt="single-product">
                                <img class="secondary-img" src="${data[i].Image}" alt="single-product">
                        </a>`;
                if (data[i].NewEn !== '') { row += `<span class="sticker-new">${data[i].NewEn}</span>`; }
                row += `</div>
                        <div class="pro-content">
                            <div class="pro-info">
                                <h4><a href="/Products/Details/${data[i].PId}">${data[i].VehicleInfoEn}</a></h4>
                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                <div class="label-product l_sale"> ${Resource.Home.Certified} </div>
                            </div>
                            <div class="pro-actions">
                                 <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                            </div>
                        </div>
                    </div>`;
                $('#Certificatesd-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlAccessory(data, selector) {
    $("#AccessoriesLoder").html("");
    $("#AccessoriesCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            priceConvertO = await this.ConvertO(data[i].AdPrice);
            row = '';
            row += `
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Images}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Images}" alt="single-product">
                            </a>`;
            if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
            row += `</div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content">
                                <div class="pro-info">
                                    <h4><a href="/Products/Details/${data[i].PId}">${data[i].AccessoryTitleAR}</a></h4>
                                    <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>`;
            $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].AdPrice);

                row = '';
                row += `
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Images}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Images}" alt="single-product">
                                </a>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content">
                                <div class="pro-info">
                                    <h4><a href="/Products/Details/${data[i].PId}">${data[i].AccessoryTitleAR}</a></h4>
                                    <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                 </a>`;
                if (data[i].NewAR !== '') { row += `<span class="sticker-new">${data[i].NewAR}</span>`; }
                row += `</div>
                                <div class="pro-actions">
                                     <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>`;
                $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                priceConvertO = await this.ConvertO(data[i].AdPrice);

                row = '';
                row += `
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Images}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Images}" alt="single-product">
                                 </a>`;
                if (data[i].NewEn !== '') { row += `<span class="sticker-new">${data[i].NewEn}</span>`; }
                row += `</div>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content">
                                <div class="pro-info">
                                    <h4><a href="/Products/Details/${data[i].PId}">${data[i].AccessoryTitleEN}</a></h4>
                                    <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                </div>
                                <div class="pro-actions">
                                     <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>`;
                $(selector).owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlBlogs(data) {
    $("#BlogsLoder").html("");
    $("#BlogsCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            row = '';
            today = new Date(data[i].blogDate);
            D = String(today.getDate()).padStart(2, '0');
            M = String(today.getMonth() + 1).padStart(2, '0');
            row += `<div class="single-latest-blog red-border">`;
            row += `<div class="blog-img"><a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a></div>`;
                row += `<div class="blog-desc">`;
            row += `<h4><a href="/blogs/Dtails/${data[i].blogId}>${data[i].blogTitleAr}</a></h4><p>${data[i].blogBodyAr.replace(/<(.|\n)*?>/g, '')}</p><a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>`;
                    row += `<div class="blog-date"><span>${D}</span>${M}</div>`;
                row += `</div>`;
            row += `</div>`;
            $('#blogs-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                row = '';
                today = new Date(data[i].Date);
                D = String(today.getDate()).padStart(2, '0');
                M = String(today.getMonth() + 1).padStart(2, '0');
                row += `<div class="single-latest-blog red-border">`;
                row += `<div class="blog-img"><a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a></div>`;
                row += `<div class="blog-desc">`;
                row += `<h4><a href="/blogs/Dtails/${data[i].blogId}>${data[i].blogTitleAr}</a></h4><p>${data[i].blogBodyAr.replace(/<(.|\n)*?>/g, '')}</p><a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>`;
                row += `<div class="blog-date"><span>${D}</span>${M}</div>`;
                row += `</div>`;
                row += `</div>`;
                $('#blogs-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                row = '';
                today = new Date(data[i].Date);
                D = String(today.getDate()).padStart(2, '0');
                M = String(today.getMonth() + 1).padStart(2, '0');
                row += `<div class="single-latest-blog red-border">`;
                row += `<div class="blog-img"><a href="/blogs/Dtails/${data[i].blogId}"><img src="${data[i].blogIamgeUrl}" alt="blog-image"></a></div>`;
                row += `<div class="blog-desc">`;
                row += `<h4><a href="/blogs/Dtails/${data[i].blogId}>${data[i].blogTitleEn}</a></h4><p>${data[i].blogBodyEn.replace(/<(.|\n)*?>/g, '')}</p><a class="readmore" href="/blogs/Dtails/${data[i].blogId}">${Resource.Home.ReadMore}</a>`;
                row += `<div class="blog-date"><span>${D}</span>${M}</div>`;
                row += `</div>`;
                row += `</div>`;
                $('#blogs-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');            }
        }
    }
}

async function HtmlYouTubeVideos(data) {
    $("#VideosLoder").html("");
    $("#VideosCont").show();
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            row = '';
            row += `<div class="single-latest-blog">`;
            row += `<div class="blog-img">`;
            row += `<a href="${data[i].VideoUrl}"><iframe class="video-iframe" src="${data[i].VideoUrl}"></iframe></a>`;
            row += `</div>`;

            row += `<div class="blog-desc">`;
            row += `<h4><a href="${data[i].VideoUrl}">${data[i].VideoNameAr}</a></h4> <a class="readmore" href=" ">${Resource.Home.ReadMore}</a>`;
            row += `</div>`;
            row += `</div>`;
            $('#YouTubeVideos-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
        }
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                row = '';
                row += `<div class="single-latest-blog">`;
                row += `<div class="blog-img">`;
                row += `<a href="${data[i].VideoUrl}"><iframe class="video-iframe" src="${data[i].VideoUrl}"></iframe></a>`;
                row += `</div>`;

                row += `<div class="blog-desc">`;
                row += `<h4><a href="${data[i].VideoUrl}">${data[i].VideoNameAr}</a></h4> <a class="readmore" href=" ">${Resource.Home.ReadMore}</a>`;
                row += `</div>`;
                row += `</div>`;
                $('#YouTubeVideos-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
        else {
            for (i = 0; i < data.length; i++) {
                row = '';
                row += `<div class="single-latest-blog">`;
                row += `<div class="blog-img">`;
                row += `<a href="${data[i].VideoUrl}" target="_blank"><iframe class="video-iframe" src="${data[i].VideoUrl}"></iframe></a>`;
                row += `</div>`;

                row += `<div class="blog-desc">`;
                row += `<h4><a href="${data[i].VideoUrl}">${data[i].VideoNameAr}</a></h4> <a class="readmore" href=" ">${Resource.Home.ReadMore}</a>`;
                row += `</div>`;
                row += `</div>`;
                $('#YouTubeVideos-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
            }
        }
    }
}

async function HtmlGetBrand(data) {
    $("#HotBrandsLoder").html("");
    $("#HotBrandsCont").show();
    var Group = parseInt(data.length / 3);
    var currentPage = 0;
    for (i = 0; i < Group; i++) {
        row = '';
        row += `<div class="single-brand">`;
        for (j = 1; j < 4; j++) {
            let index = parseInt(j + currentPage);
            row += `<a href="/Products/List?BrandId=${data[index - 1].BrandId}"><img class="img" src="${data[index - 1].BrandsLogo}" alt="brand-image${index - 1}" style="width:80px;height:78px"></a>`;
        }
        row += `</div>`;
        currentPage = parseInt(currentPage + 3);
        $('#Brand-data').owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
    }
    
}

async function HtmlFooterAds(data) {
    $("#AdsImageLeft").attr("src",data[0].Image);
    $("#AdsImageRight").attr("src",data[1].Image);
}