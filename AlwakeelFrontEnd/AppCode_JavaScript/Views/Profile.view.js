﻿async function HtmlColors(data) {
    $("#colorId").html("");
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].coloreId + '">' + data[i].NameAR + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].coloreId + '">' + data[i].NameAR + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].coloreId + '">' + data[i].NameEn + '</option>';
            }
        }
    }
    $("#colorId").append(row);

}
async function HtmlAccessoriesCategories(data) {
    $("#AccCatId").html("");
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].CatId + '">' + data[i].NameAr + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CatId + '">' + data[i].NameAr + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].CatId + '">' + data[i].NameEn + '</option>';
            }
        }
    }
    $("#AccCatId").append(row);

}
async function HtmlMainAccessoriesType(data) {
    $("#MainAccessoriesType").html("");
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].MainAccessoriesId + '">' + data[i].NameAR + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].MainAccessoriesId + '">' + data[i].NameAR + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].MainAccessoriesId + '">' + data[i].NameEn + '</option>';
            }
        }
    }
    $("#MainAccessoriesType").append(row);

}

async function HtmlSubAccessoriesType(data) {
    $("#SubAccessoriesType").html("");
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].SubAccessoriesId + '">' + data[i].NameAR + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].SubAccessoriesId + '">' + data[i].NameAR + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].SubAccessoriesId + '">' + data[i].NameEn + '</option>';
            }
        }
    }
    $("#SubAccessoriesType").append(row);
}

async function HtmlFeaturesModel(data) {
    $("#Features").html("");
    var row = '';
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {

        for (i; i < data.length; i++) {
            row += '<option value="' + data[i].FId + '">' + data[i].FNameAR + '</option>';
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].FId + '">' + data[i].FNameAR + '</option>';
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += '<option value="' + data[i].FId + '">' + data[i].FNameEn + '</option>';
            }
        }
    }
    $("#Features").append(row);

}

function HtmlGetMyProducts(data) {
    if (data.length === 0) {
        //msgError('no data');
    }
    else {
        $('#AdsList-data').dataTable().fnDestroy();
        $('#AdsList-data').DataTable({
            "processing": true,
            "data": data,
            "columns": [
                {
                    "data": "PId", "render": function (data) {
                        let btn_action = `
                                           <a class='btn btn-primary' href='/Products/Details/${data}' /> 
                                                ${Resource.Home.ReadMore} <i class='fa fa-info-circle'></i>
                                           </a>
                                       `;
                        return btn_action;
                    }
                },
                { "data": "Info" },
                { "data": "NewEn" }
            ]
        });
    }
}

function HtmlGetRantCars(data) {
    if (data.length === 0) {
        //msgError('no data');
    }
    else {
        $('#RentCars-data').dataTable().fnDestroy();

        if (typeof $.cookie('Language') === 'undefined') {
            $('#RentCars-data').DataTable({
                "processing": true,
                "data": data,
                "columns": [
                    { "data": "CarInfoAR" },
                    {
                        "data": "ImageURl", "render": function (data) {
                            let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                            return btn_action;
                        }
                    }
                ]
            });
        } else {
            var lang = $.cookie('Language');
            if (lang === 'ar') {
                $('#RentCars-data').DataTable({
                    "processing": true,
                    "data": data,
                    "columns": [
                        { "data": "CarInfoAR" },
                        {
                            "data": "ImageURl", "render": function (data) {
                                let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                                return btn_action;
                            }
                        }
                    ]
                });
            } else {
                $('#RentCars-data').DataTable({
                    "processing": true,
                    "data": data,
                    "columns": [
                        { "data": "CarInfoEn" },
                        {
                            "data": "ImageURl", "render": function (data) {
                                let btn_action = `
                                          <img src="${data}" style="width:70px;height:70px"/>
                                       `;
                                return btn_action;
                            }
                        }
                    ]
                });
            }
        }
    }
}