﻿//Start Prodects List
function HtmlGetCategories(data) {
    $("#CategoriesList").html('');
    row = '';
    i = 0;
    if (typeof $.cookie('Language') === 'undefined') {
        for (i; i < data.length; i++) {
            row += `<li class="form-check">
                <input class="form-check-input GetCat" value="${data[i].CatId}" id="box${data[i].CatId}" type="checkbox">
                <label class="form-check-label" for="camera">${data[i].NameAr}</label>
            </li>`;
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i; i < data.length; i++) {
                row += `<li class="form-check">
                <input class="form-check-input GetCat" value="${data[i].CatId}" id="box${data[i].CatId}" type="checkbox">
                <label class="form-check-label" for="camera">${data[i].NameAr}</label>
            </li>`;
            }
        }
        else {
            for (i; i < data.length; i++) {
                row += `<li class="form-check">
                <input class="form-check-input GetCat" value="${data[i].CatId}" id="box${data[i].CatId}" type="checkbox">
                <label class="form-check-label" for="camera">${data[i].NameEn}</label>
            </li>`;
            }
        }
    }
    $("#CategoriesList").append(row);
}

function HtmlGetAccessoresslist(data) {
    $("#Accessoress-list").html('');
    var row = '';
    i = 0;
    j = 0;

    row += '<div id="shop-cate-toggle" class="category-menu sidebar-menu sidbar-style"><ul>';
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            row += `<li class="has-sub">
                        <a href="#">${data[i].NameAR}</a>
                        <ul class="category-sub">`;
            for (j = 0; j < data[i].SubList.length; j++) {
                row += `<li><a id="${data[i].SubList[j].SubAccessoriesId}" class="subAccessorie">${data[i].SubList[j].NameAR}</a></li>`;
            }
            row += `</ul></li>`;
        }
    } else {
        var lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                row += `<li class="has-sub">
                        <a href="#">${data[i].NameAR}</a>
                        <ul class="category-sub">`;
                for (j = 0; j < data[i].SubList.length; j++) {
                    row += `<li><a id="${data[i].SubList[j].SubAccessoriesId}" class="subAccessorie">${data[i].SubList[j].NameAR}</a></li>`;
                }
                row += `</ul></li>`;
            }
        } else {
            for (i = 0; i < data.length; i++) {
                row += `<li class="has-sub">
                            <a href="#">${data[i].NameEn}</a>
                        <ul class="category-sub">`;
                for (j = 0; j < data[i].SubList.length; j++) {
                    row += `<li><a id="${data[i].SubList[j].SubAccessoriesId}" class="subAccessorie">${data[i].SubList[j].NameEn}</a></li>`;
                }
                row += `</ul></li>`;
            }
        }
    }
    row += '</div></div>';
    $("#Accessoress-list").append(row);

}
function HtmlGetProducts(data) {
    var List = [];
    var loadData;
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {
        for (i = 0; i < data.length; i++) {
            loadData = {
                "PId": data[i].PId,
                "Category": data[i].Category,
                "CatId": data[i].CatId,
                "Name": data[i].NameAR,
                "Price": data[i].Price,
                "Images": data[i].Images,
                "New": data[i].NewAR,
                "Views": data[i].Views,
                "love": data[i].love
            };
            List.push(loadData);
        }
        HtmlViewerProducts(List);
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            for (i = 0; i < data.length; i++) {
                loadData = {
                    "PId": data[i].PId,
                    "Category": data[i].Category,
                    "CatId": data[i].CatId,
                    "Name": data[i].NameAR,
                    "Price": data[i].Price,
                    "Images": data[i].Images,
                    "New": data[i].NewAR,
                    "Views": data[i].Views,
                    "love": data[i].love
                };
                List.push(loadData);
            }
            HtmlViewerProducts(List);
        } else {
            for (i = 0; i < data.length; i++) {
                loadData = {
                    "PId": data[i].PId,
                    "Category": data[i].Category,
                    "CatId": data[i].CatId,
                    "Name": data[i].NameEN,
                    "Price": data[i].Price,
                    "Images": data[i].Images,
                    "New": data[i].NewEn,
                    "Views": data[i].Views,
                    "love": data[i].love
                };
                List.push(loadData);
            }
            HtmlViewerProducts(List);
        }
    }
}
async function HtmlViewerProducts(data) {
    row = '';
    row1 = '';
    i = 0;
    if (parseInt(data.length) !== 0) {
        $("#no-more").fadeOut();
        $(".load_more_animation").fadeOut();
        // lazy load 10 data from server 
        // if length of data its grater than or equale 10 then we can load more data from server 
        if (parseInt(data.length) >= 12) {
            $("#load-more").fadeIn();
        } else {
            $("#load-more").fadeOut();
        }
        // latest length of data 
        let len = parseInt($("#lastLentgh").val()) + parseInt(data.length);
        // chanage the value of hidden input to store last length of data load from server
        $("#lastLentgh").val(len);
        for (i = 0; i < data.length; i++) {
            priceConvertO = await this.ConvertO(data[i].Price);

            row += `<div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                    <div class="single-product">
                                        <div class="pro-img">
                                            <a href="/Products/Details/${data[i].PId}/${data[i].Name.split(' ').join('_')}">
                                                <img class ="primary-img" src="${data[i].Images}" alt="single-product">
                                                <img class ="secondary-img" src="${data[i].Images}" alt="single-product">
                                            </a>
                                        </div>
                                        <div class="pro-content">
                                            <div class="pro-info">
                                                <h4><a href="/Products/Details/${data[i].PId}">${data[i].Name}</a></h4>
                                                <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                            </div>
                                            <div class="pro-actions">
                                                 <div class="actions-secondary">
                                                        <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                                        <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                                </div>
                                                <div class="actions-primary">
                                                    <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                                </div>
                                            </div>
                                        </div>`;
            if (data[i].New !== '') { row += `<span class="sticker-new">${data[i].New}</span>`; }
            row += `</div></div>`;

            row1 += `<div class="single-product">
                                <div class="row">
                                    <!-- Product Image Start -->
                                    <div class="col-lg-4 col-md-5 col-sm-12">
                                        <div class="pro-img">
                                            <a href="/Products/Details/${data[i].PId}">
                                                <img class="primary-img" src="${data[i].Images}" alt="single-product">
                                                <img class="secondary-img" src="${data[i].Images}" alt="single-product">
                                            </a>`;
            if (data[i].New !== '') { row1 += `<span class="sticker-new">${data[i].New}</span>`; }
            row1 += `</div>
                                    </div>
                                    <!-- Product Image End -->
                                    <!-- Product Content Start -->
                                    <div class="col-lg-8 col-md-7 col-sm-12">
                                        <div class="pro-content hot-product2">
                                            <h4><a href="/Products/Details/${data[i].PId}">${data[i].Name}</a></h4>
                                            <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    <a href="cart.html" title="" data-original-title="Add to Cart"> + Add To Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Product Content End -->
                                </div>
                            </div>`;
        }
        $("#ProductList-data").append(row);
        $("#ProductListView-data").append(row1);
    } else {
        $("#no-more").fadeIn();
        $("#load-more").fadeOut();
    }
}
//End Prodects List

//Start Prodect Details
function HtmlGetPorductDetails(data) {
    $("#ProDetailLoder").html("");
    $("#ProDetail").show();

    $("#CarsFeatureLoder").html("");
    $("#CarsFeature").show();
    var PorductInfo;
    var item;
    var itemSimilar;
    var CarsFeatureInfo = [];
    var CarsSimilarAds = [];
    var i = 0;
    if (typeof $.cookie('Language') === 'undefined') {
        PorductInfo = {
            "Id": data.PId,
            "CategoryId": data.CatId,
            "CategoryName": data.Category,
            "ProdectName": data.NameAR,
            "Price": data.Price,
            "Images": data.Images,
            "New": data.NewAR,
            "Describe": data.DescribeAR,
            "Views": data.Views,
            "love": data.love,
            "ColorIcon": data.ColorIcon,
            "Color": data.ColorAR,
            "EngineCapacity": data.EngineCapacityCC,
            "MileageKM": data.MileageKM,
            "Logo": data.Logo,
            "UserName": data.ownerNameAR,
            "businessId": data.businessId
        };
        HtmlViewerPorductDetails(PorductInfo);
    } else {
        lang = $.cookie('Language');
        if (lang === 'ar') {
            PorductInfo = {
                "Id": data.PId,
                "CategoryId": data.CatId,
                "CategoryName": data.Category,
                "ProdectName": data.NameAR,
                "Price": data.Price,
                "Images": data.Images,
                "New": data.NewAR,
                "Describe": data.DescribeAR,
                "Views": data.Views,
                "love": data.love,
                "ColorIcon": data.ColorIcon,
                "Color": data.ColorAR,
                "EngineCapacity": data.EngineCapacityCC,
                "MileageKM": data.MileageKM,
                "Logo": data.Logo,
                "UserName": data.ownerNameAR,
                "businessId": data.businessId
            };
            HtmlViewerPorductDetails(PorductInfo);
        } else {
            PorductInfo = {
                "Id": data.PId,
                "CategoryId": data.CatId,
                "CategoryName": data.Category,
                "ProdectName": data.NameEN,
                "Price": data.Price,
                "Images": data.Images,
                "New": data.NewAR,
                "Describe": data.DescribeEN,
                "Views": data.Views,
                "love": data.love,
                "ColorIcon": data.ColorIcon,
                "Color": data.ColorEn,
                "EngineCapacity": data.EngineCapacityCC,
                "MileageKM": data.MileageKM,
                "Logo": data.Logo,
                "UserName": data.ownerNameEn,
                "businessId": data.businessId
            };
            HtmlViewerPorductDetails(PorductInfo);
        }
    }
    ///Cars Feature
    for (i = 0; i < data.CarsFeature.length; i++) {
        if (typeof $.cookie('Language') === 'undefined') {
            item = {
                "Feature": data.CarsFeature[i].FeatureAr,
                "Icon": data.CarsFeature[i].Icons
            };
            CarsFeatureInfo.push(item);
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                item = {
                    "Feature": data.CarsFeature[i].FeatureAr,
                    "Icon": data.CarsFeature[i].Icons
                };
                CarsFeatureInfo.push(item);
            } else {
                item = {
                    "Feature": data.CarsFeature[i].FeatureEn,
                    "Icon": data.CarsFeature[i].Icons
                };
                CarsFeatureInfo.push(item);
            }
        }
    }
    HtmlViewerCarsFeature(CarsFeatureInfo);
    // onar contacts
    HtmlViewerContacts(data.contacts);
    ///Similar Ads
    for (i = 0; i < data.SimilarAds.length; i++) {
        if (typeof $.cookie('Language') === 'undefined') {
            itemSimilar = {
                "CatId": data.SimilarAds[i].CatId,
                "Category": data.SimilarAds[i].Category,
                "Images": data.SimilarAds[i].Images,
                "Name": data.SimilarAds[i].NameAR,
                "New": data.SimilarAds[i].NewAR,
                "PId": data.SimilarAds[i].PId,
                "Price": data.SimilarAds[i].Price,
                "Views": data.SimilarAds[i].Views,
                "love": data.SimilarAds[i].love
            };
            CarsSimilarAds.push(itemSimilar);
        } else {
            lang = $.cookie('Language');
            if (lang === 'ar') {
                itemSimilar = {
                    "CatId": data.SimilarAds[i].CatId,
                    "Category": data.SimilarAds[i].Category,
                    "Images": data.SimilarAds[i].Images,
                    "Name": data.SimilarAds[i].NameAR,
                    "New": data.SimilarAds[i].NewAR,
                    "PId": data.SimilarAds[i].PId,
                    "Price": data.SimilarAds[i].Price,
                    "Views": data.SimilarAds[i].Views,
                    "love": data.SimilarAds[i].love
                };
                CarsSimilarAds.push(itemSimilar);
            } else {
                itemSimilar = {
                    "CatId": data.SimilarAds[i].CatId,
                    "Category": data.SimilarAds[i].Category,
                    "Images": data.SimilarAds[i].Images,
                    "Name": data.SimilarAds[i].NameEN,
                    "New": data.SimilarAds[i].NewEn,
                    "PId": data.SimilarAds[i].PId,
                    "Price": data.SimilarAds[i].Price,
                    "Views": data.SimilarAds[i].Views,
                    "love": data.SimilarAds[i].love
                };
                CarsSimilarAds.push(itemSimilar);
            }
        }
    }
    HtmlVieweSimilarAds(CarsSimilarAds);
}
//End Prodect Details
async function HtmlViewerPorductDetails(data) {
    console.log(data.CategoryName);
    $("#Prod-Link").attr("href", `/Products/Details/${data.Id}/${data.ProdectName.split(' ').join('_')}`);
    $("#Prod-Link").text(data.ProdectName);
    priceConvertO = await this.ConvertO(data.Price);

    var i = 0;
    row = '';
    row = `
            <div class="thubnail-desc fix">
                                <h3 class="product-header">${data.ProdectName}</h3>
                                
                                <div class="pro-price mtb-30">
                                    <p class="d-flex align-items-center"><span class="prev-price"> ${priceConvertO} ${Resource.Home.SDG}</span><span class="price">${priceConvertO} ${Resource.Home.SDG}</span><span class="saving-price">${data.New}</span></p>
                                </div>
                                <p class="mb-20 pro-desc-details">${data.Describe}</p>
                                <div class="color clearfix mb-20">`;
    if (data.Logo === "") {
        row += `<label> ${Resource.PorductDetails.Color}</label>
                                    <ul class="color-list">
                                        <li>
                                            <a class="" active" href="#"><img width="30" height="30" style="border:solid 2px #f1f1f1" src="${data.ColorIcon}" /></a>
                                        </li>
                                    </ul >`;
    }

    row += `</div>
                                <div class="pro-ref mt-20">
                                    <div class="row">
                                        <div class="col-lg-3">${Resource.PorductDetails.Views} <p><span class="in-stock"><i class="ion-eye"></i> ${data.Views}</span></p></div>
                                        <div class="col-lg-3">${Resource.Home.AddToWishListBtn} <p><span id="Lov_${data.PId}" class="in-stock"><i class="ion-heart"></i> ${data.love}</span></p></div>
                                        <div class="col-lg-3">${Resource.PorductDetails.EngineCapacity}<p><span class="in-stock"><i class="ion-checkmark-round"></i> ${data.EngineCapacity}</span></p></div>
                                        <div class="col-lg-3">${Resource.PorductDetails.MileageKM}<p><span class="in-stock"><i class="ion-checkmark-round"></i> ${data.MileageKM}</span></p></div>
                                    </div>
                                </div>
                            </div>`;
    $("#ProdectInfo").html(row);

    //slider
    row = '';
    row1 = '';
    row = `<div id="thumb0" class="tab-pane fade show active">
                <a data-fancybox="images" href="${data.Images[0]}"><img src="${data.Images[0]}" alt="product-view"></a>
            </div>`;
    row1 = `<a class="active" data-toggle="tab" href="#thumb0"><img src="${data.Images[0]}" alt="product-thumbnail"></a>`;

    $("#Image_slider").owlCarousel().trigger('add.owl.carousel', [jQuery(row1)]).trigger('refresh.owl.carousel');
    var newLength = parseInt(data.Images.length - 2);
    for (i = 0; i < newLength; i++) {
        row += `<div id="thumb${i}" class="tab-pane fade">
                    <a data-fancybox="images" href="${data.Images[i]}"><img src="${data.Images[i]}" alt="product-view"></a>
               </div>`;
        row1 = `<a data-toggle="tab" href="#thumb${i}"><img src="${data.Images[i]}" alt="product-thumbnail"></a>`;
        $("#Image_slider").owlCarousel().trigger('add.owl.carousel', [jQuery(row1)]).trigger('refresh.owl.carousel');
    }
    $("#Image_fancybox").html(row);
    ///User Profile
    $("#UserIcon").attr("src", data.Logo);
    $("#UserId").attr("href", "/Companies/Viewditels/" + data.businessId);

    $("#UserName").text(data.UserName);
    $("#UserType").text(data.CategoryName);

}
//End Cars Feature
function HtmlViewerCarsFeature(data) {
    row = '';
    i = 0;
    $("#FeatureDetails").text(Resource.PorductDetails.FeatureDetails);
    $("#CarsFeature").html("");
    for (i = 0; i < data.length; i++) {
        row += `<div class="col-lg-3"><p><span class="in-stock"><img src="${data[i].Icon}" class="FeatureIcon" /> ${data[i].Feature}</span></p></div>`;
    }
    $("#CarsFeature").html(row);
}
//End Contacts
function HtmlViewerContacts(data) {
    row = '';
    i = 0;
    $("#Contacts_data").html("");
    row += `<li class="first"> ${Resource.PorductDetails.Contacts} </li>`;
    for (var i = 0; i < data.length; i++) {
        row += `<li><a href="" target="_blank"><i class="${data[i].Icons}" aria-hidden="true"></i> ${data[i].contact} </a></li>`;
    }
    $("#Contacts_data").html(row);

}

//
async function HtmlVieweSimilarAds(data) {
    $("#SimilarAdsLoder").html("");
    $("#SimilarAdsCont").show();
    for (i = 0; i < data.length; i++) {
        priceConvertO = await this.ConvertO(data[i].Price);
        row = '';
        row += `<div class="single-product">
                            <div class="pro-img">
                                <a href="/Products/Details/${data[i].PId}">
                                    <img class="primary-img" src="${data[i].Images}" alt="single-product">
                                    <img class="secondary-img" src="${data[i].Images}" alt="single-product">
                                </a>`;
                if (data[i].New !== '') { row += `<span class="sticker-new">${data[i].New}</span>`; }
                row += `</div>
                            <div class="pro-content">
                                <div class="pro-info">
                                     <h4><a href="/Products/Details/${data[i].PId}">${data[i].Name}</a></h4>
                                     <p><span class="price">${priceConvertO} ${Resource.Home.SDG}</span></p>
                                </div>
                                <div class="pro-actions">
                                    <div class="actions-secondary">
                                            <div class="col-lg-6"> <p><span class="in-stock"><i class="ion-eye"></i>${data[i].Views}</span></p></div>
                                            <div class="col-lg-6"> <p><span id="Lov_${data[i].PId}" class="in-stock Love-ico"><i class="ion-heart Love-ico"></i>${data[i].love}</span></p></div>
                                    </div>
                                    <div class="actions-primary">
                                        <a id="${data[i].PId}" title="${Resource.Home.AddToWishListBtn}" class="LikeBtn"><i class="lnr lnr-heart"></i> <span>${Resource.Home.AddToWishListBtn}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>`;
        $("#SimilarAds-data").owlCarousel().trigger('add.owl.carousel', [jQuery(row)]).trigger('refresh.owl.carousel');
    }
}
