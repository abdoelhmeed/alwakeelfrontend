﻿//Get
async function Get(actionType, action, data, FunName, selector = null, selector2 = null) {
    //salert(FunName);
    $.ajax({
        url: Url + action,
        type: actionType,
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $("#conloading").hide();
            $("#Div-Loder").html("");
            switch (FunName) {
                case 'GetHeaderAds':
                    HtmlHeaderAds(data);
                    break;
                case 'CategoriesFilter':
                    HtmlCategoriesFilter(data);
                    break;
                case 'BrandFilter':
                    HtmlBrandFilter(data);
                    break;
                case 'ClassFilter':
                    HtmlClassFilter(data);
                    break;
                case 'ModelFilter':
                    HtmlModelFilter(data);
                    break;
                case 'Colors':
                    HtmlColors(data);
                    break;
                case 'Slider':
                    HtmlSlider(data);
                    break;
                case 'GetMiddleAds':
                    HtmlMiddleAds(data);
                    break;
                case 'OurPartners':
                    HtmlOurPartners(data);
                    HtmlMenuOurPartners(data);
                    break;
                case 'GetFeatured':
                    HtmlFeatured(data, selector);
                    break;
                case 'GetDealOfday':
                    HtmlDealOfday(data);
                    break;
                case 'GetCertificatesd':
                    HtmlCertificatesd(data);
                    break;
                case 'Accessory':
                    HtmlAccessory(data, selector);
                    break;
                case 'Blogs':
                    HtmlBlogs(data);
                    break;
                case 'YouTubeVideos':
                    HtmlYouTubeVideos(data);
                    break;
                case 'GetBrand':
                    HtmlGetBrand(data);
                    break;
                case 'GetFooterAds':
                    HtmlFooterAds(data);
                    break;
                //End Home Page
                case 'GetCompanies':
                        HtmlGetCompanies(data, selector);
                    break;
                case 'GetPartCompanies':
                    HtmlGetPartCompanies(data);
                    break;
                case 'GetInsuranceById':
                    HtmlGetInsuranceById(data);
                    break;
                //End Companies
                //Start Product
                case 'CategoriesList':
                    HtmlGetCategories(data);
                    break;
                case 'Accessoresslist':
                    HtmlGetAccessoresslist(data);
                    break;
                case 'ProductView':
                    if (selector == false && selector2 === false) {
                        $("#ProductsLoder").hide();
                        $("#ProductList-data,#ProductListView-data").show();
                        $("#ProductList-data,#ProductListView-data").html("");
                        console.log("مندون فلتر ولا لود كورر");
                    }
                    if (selector == true && selector2 === false) {
                        $("#ProductsLoder").hide();
                        $("#ProductList-data,#ProductListView-data").show();
                        $("#ProductList-data,#ProductListView-data").html("");
                        console.log(" فلتر و مندون لود كورر");

                    }
                    HtmlGetProducts(data);
                    break;
                case 'PorductDetails':
                    HtmlGetPorductDetails(data);
                    break;
                //Start Rent Car
                case 'RentCarList':
                    HtmlGetRentCarList(data);
                    break;
                case 'SearchRentCarList':
                    HtmlGetSearchRentCarList(data);
                    break;
                //End Rent Car
                //Start About
                case 'GetAboutAlwakeel':
                    HtmlGetAboutAlwakeel(data);
                    break;
                //End About
                //Start Blog
                case 'GetBlog':
                    HtmlGetBlog(data);
                    break;
                case 'GetBlogdetails':
                    HtmlGetBlogdetails(data);
                    break;
                case 'Like':
                    if (data.Status === "success") {
                        var Current = parseInt($("#Lov_" + selector).text());
                        Current = Current + 1;
                        $("#Lov_" + selector).html("<i class='ion-heart Love-ico'></i> " + Current);
                        msgSuccess(data.MessageAr + ' - ' + data.MessageEn);
                    }
                    break;
                case 'FeaturesModel':
                    HtmlFeaturesModel(data);
                    break;
                case 'AccessoriesCategories':
                    HtmlAccessoriesCategories(data);
                    break;
                default:
                //do block code
            }
        },
        error: function (responce, httpObj, textStatus) {
            $("#conloading").hide();
            Url === Router.BaseUrl.online + "api/" ? "" : msgError(textStatus);
            console.log(responce);
        }
    });
}

//Get Auth
function GetAuth(actionType, action, token, data, FunName) {
    $.ajax({
        url: Url + action,
        type: actionType,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $("#conloading").hide();
            switch (FunName) {
                case 'GetMyProducts':
                    HtmlGetMyProducts(data);
                    break;
                case 'GetRantCars':
                    HtmlGetRantCars(data);
                    break;
               
                case 'MainAccessoriesType':
                    HtmlMainAccessoriesType(data);
                    break;
                case 'SubAccessoriesType':
                    HtmlSubAccessoriesType(data);
                    break;
                case 'CategoriesFilter':
                    HtmlCategoriesFilter(data);
                    break;
                default:
                // code block
            }
        },
        error: function (responce, httpObj, textStatus) {
            if (responce.status === 401) {
                $.removeCookie('UserToken', { path: '/' });
                window.location.href = '/Account/Login';
            } else {
                $("#conloading").hide();
                Url === Router.BaseUrl.online + "api/" ? "" : msgError(responce.responseText);
            }
        }
    });
}

//POST
function POST(actionType, action, data, FunName) {
    $.ajax({
        type: actionType,
        url: Url + action,
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        processData: false,
        success: function (responce) {
            $("#CustomPreloader").hide();
            switch (FunName) {
                case 'ResponceFromServer':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    break;
                case 'register':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                        var LoginType = $("#LoginType").val();
                        if (LoginType === "phone") {
                            $.cookie('Confirmation', $("#phone").val());
                        } else {
                            $.cookie('Confirmation', $("#email").val());
                        }
                        setTimeout(function () {
                            window.location.href = '/Account/Verification';
                        }, 100);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    break;
                case 'Login':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                        var logby = $("#LoginBy").val();
                        if (logby === "phone") {
                            $.cookie('PhoneNumberConfirmation', $("#phone").val());
                        } else {
                            $.cookie('EmailConfirmation', $("#email").val());
                        }
                        setTimeout(function () {
                            window.location.href = '/Account/Verification';
                        }, 100);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    break;
                case 'Verification':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                        $.cookie('UserToken', JSON.stringify(responce.token), { expires: responce.expires_in, path: '/' });
                        $.removeCookie("Confirmation");
                        setTimeout(function () {
                            window.location.href = '/Account/Profile';
                        }, 100);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    break;
                
                default:
                // code block
            }
        },
        error: function (responce, httpObj, textStatus) {
            $("#CustomPreloader").hide();
            Url === Router.BaseUrl.online + "api/" ? "" : msgError(responce.responseText);
        }
    });
}

//POST Auth
function POSTAuth(actionType, action, token, data, FunName) {
    $.ajax({
        type: actionType,
        url: Url + action,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        processData: false,
        success: function (responce) {
            $("#CustomPreloader").hide();
            switch (FunName) {
                case 'ResponceFromServer':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    window.location.href = '/Account/Profile';
                    break;
                case 'CreateNewAds':
                    if (responce.Status === "success") {
                        msgSuccess(responce.MessageAr);
                    } else {
                        msgError(responce.MessageAr);
                    }
                    break;
                default:
                // code block
            }
        },
        error: function (responce, httpObj, textStatus) {
            if (responce.status === 401) {
                $.removeCookie('UserToken', { path: '/' });
                window.location.href = '/Account/Login';
            } else {
                $("#conloading").hide();
                Url === Router.BaseUrl.online + "api/" ? "" : msgError(responce.responseText);
            }
        }
    });
}
