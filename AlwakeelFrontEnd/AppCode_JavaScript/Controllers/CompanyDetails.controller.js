﻿$(document).ready(function () {
    var hashes = window.location.href.slice(window.location.href.indexOf('/') + 1).split('/');
    var CompId = hashes[4];
    $("#Comp-Name").text("...");
    ListOfCompanies();

    function ListOfCompanies() {
        Get("GET", Router.Companies.GetInsuranceById, { businessId: CompId }, "GetInsuranceById");
    }
    $("#OrderNow-form").submit(function (event) {
        event.preventDefault();
        $("#CustomPreloader").show();
        var name = $("#name").val();
        var phone = $("#phone").val();
        var Product = $("#Product").val();
        var Quantity = $("#Quantity").val();
        var Description = $("#Description").val();
        if (CompId !== "") {
            var data = {
                "BusinessId": CompId,
                "EmpName": name,
                "contact": phone,
                "Product": Product,
                "Quantity": Quantity,
                "DescriptionOrMote": Description
            };
            POST('POST', Router.Companies.PostNewBayNow, data, 'ResponceFromServer');
            $('#OrderNowModal').modal('hide');
        } else {
            msgError(Resource.RentCar.SelectCar);
        }
    });


    $(document).on('click', '.GetProdectBrand', function () {
        var id = $(this).attr("Id");
        if (id !== "") {
            window.location = '/Products/List?filter=company&BId=' + CompId + '&BrandId=' + id;
        }
    });
});