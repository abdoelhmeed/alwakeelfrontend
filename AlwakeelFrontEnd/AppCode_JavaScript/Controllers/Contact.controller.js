﻿$(document).ready(function () {
    $("#contact-form").submit(function (event) {
            event.preventDefault();
            Swal.fire(
                {
                    title: Resource.Register.title,
                    text: Resource.Register.text,
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: Resource.Contact.confirmButtonText,
                    cancelButtonText: Resource.Contact.cancelButtonText,
                    confirmButtonClass: "btn btn-success mt-2",
                    cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
                }).then(
                    function (t) {
                        if (t.value) {
                            var data = {
                                "FullName": $("#name").val(),
                                "Phone": $("#phone").val(),
                                "Email": $("#email").val(),
                                "Title": $("#subject").val(),
                                "subject": $("#ContMessage").val()
                            };
                            POST("POST", Router.Home.PostContactAlwakeel, data, "ResponceFromServer");
                        }
                        else {
                            Cancelled();
                        }
                    });
        });
});