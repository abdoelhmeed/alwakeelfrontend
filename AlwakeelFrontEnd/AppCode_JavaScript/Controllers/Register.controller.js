﻿jQuery(document).ready(function (i) {
    $("#LoginType").val('e');
    $('input:radio[name="ByEmail"]').change(
        function () {
            if (this.checked) {
                $(".EmailBox").show();
                $(".PhoneNumberBox").hide();
                $("#ByPhone").prop('checked', false);
                $("#LoginType").val('e');
                $("#phone").val('');
            }
        });
    $('input:radio[name="ByPhone"]').change(
        function () {
            if (this.checked) {
                $(".EmailBox").hide();
                $(".PhoneNumberBox").show();
                $("#ByEmail").prop('checked', false); 
                $("#LoginType").val('p');
                $("#email").val('');
            }
        });

    //submit data
    $(".form-register").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
                function (t) {
                    if (t.value) {
                        $("#CustomPreloader").show();
                        var data = {
                            "FullName": $("#name").val(),
                            "Email": $("#email").val() === "" ? null : $("#email").val(),
                            "PhoneNumber": $("#phone").val() === "" ? null : $("#phone").val(),
                            "LoginType": $("#LoginType").val()
                        };
                        POST('POST', Router.Account.Register, data, 'register');
                    }
                    else {
                        Cancelled();
                    }
                });
    });
});