﻿$(document).ready(function () {
    AboutAlwakeel();
    async function AboutAlwakeel() {
        $("#AboutLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        $("#AboutBox").hide();

        setTimeout(function () {
            Get("GET", Router.Home.GetAboutAlwakeel, null, "GetAboutAlwakeel");
        }, 100);
    }
});