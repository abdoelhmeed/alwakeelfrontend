﻿jQuery(document).ready(function (i) {
    //submit data
    $("#form-Verification").submit(function (event) {
        event.preventDefault();
        $("#CustomPreloader").show();

        var data = {
            "PhoneNumber": $.cookie("PhoneNumberConfirmation"),
            "Email": $.cookie("EmailConfirmation"),
            "Code": $("#Code").val()
        };
        POST('POST', Router.Account.Verification, data, 'Verification');
    });
});