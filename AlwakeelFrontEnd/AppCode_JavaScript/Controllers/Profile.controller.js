﻿$(document).ready(function () {
    if (typeof $.cookie('UserToken') !== 'undefined') {
        var data = JSON.parse($.cookie("UserToken"));
        var token = data.access_token;
        var Name = data.FullName;
        var Contact = data.contact;
        $("#UserName").text(Name);
        $("#Contact").text(Contact);
        IntDataTable("#AdsList-data");
        GetProducts();
        GetRantCars();
        CategoriesFilter();
        ColorsFilter();
        displaySelectedModels();
        GetMainAccessories();
        AccessoriesCategories();
    }

    function GetProducts() {
        GetAuth("GET", Router.CustomerProfile.GetMyProducts, token, null, "GetMyProducts");
    }
    function GetRantCars() {
        GetAuth("GET", Router.CustomerProfile.GetMyRentCars, token, null, "GetRantCars");
    }
    async function CategoriesFilter() {
        GetAuth("GET", Router.Shared.Categories, null, "CategoriesFilter");
    }
    async function AccessoriesCategories() {
        Get("GET", Router.Shared.AccessoriesCategories, null, "AccessoriesCategories");
    }
    async function ColorsFilter() {
        Get("GET", Router.Shared.Colors, null, "Colors");
    }
    function GetMainAccessories() {
        GetAuth("GET", Router.CustomerProfile.GetMainAccessoriesType, token, null, "MainAccessoriesType");
    }
    $(document).on('change', '#MainAccessoriesType', function () {
        var id = $("#MainAccessoriesType").val();
        GetAuth("GET", Router.CustomerProfile.GetSubAccessoriesType, token, { MainAccessoriesType: id }, "SubAccessoriesType");
    });

    $(document).on('change', '#RantCatDrop,#CatId,#SelectedMCatId', function () {
        var id = $("#RantCatDrop").val();
        var Cid = $("#CatId").val();
        var SMid = $("#SelectedMCatId").val();
        
        if (id !== "") {
            Get('GET', Router.Shared.Brand, { CatId: id }, 'BrandFilter');
        }
        else
        if (Cid !== "") {
            Get('GET', Router.Shared.Brand, { CatId: Cid }, 'BrandFilter');
        }
        else
        if (SMid !== "") {
            Get('GET', Router.Shared.Brand, { CatId: SMid }, 'BrandFilter');
        }
       
    });
    $(document).on('change', '#RantPrandDrop,#BrandId,#SelectedMBrandId', function () {
        var id = $("#RantPrandDrop").val();
        var Cid = $("#BrandId").val();
        var SMid = $("#SelectedMBrandId").val();
        if (id !== "") {
            Get('GET', Router.Shared.Class, { BrandId: id }, 'ClassFilter');
        }
        else
        if (Cid !== "") {
            Get('GET', Router.Shared.Class, { BrandId: Cid }, 'ClassFilter');
        }
        else
        if (SMid !== "") {
            Get('GET', Router.Shared.Class, { BrandId: SMid }, 'ClassFilter');
        }
        else {
            //msgError("select section first");
        }
    });
    $(document).on('change', '#RantClassDrop,#CarClassId,#SelectedMClassId', function () {
        var id = $("#RantClassDrop").val();
        var Cid = $("#CarClassId").val();
        var SMid = $("#SelectedMClassId").val();
        
        if (id !== "") {
            Get('GET', Router.Shared.Model, { CarClassId: id }, 'ModelFilter');
        }
        else
        if (Cid !== "") {
            Get('GET', Router.Shared.Model, { CarClassId: Cid }, 'ModelFilter');
            }
        else
            if (SMid !== "") {
                Get('GET', Router.Shared.Model, { CarClassId: SMid }, 'ModelFilter');
        }
    });
    $(document).on('change', '#ModelId', function () {
        var id = $("#ModelId").val();
        Get("GET", Router.Shared.GetFeatures, { modelId: id }, "FeaturesModel");
    });
    //////////////////////////////////////////////////
    $(document).on('click', '#NewAds-tab', function () {
        $("#BNewAds").attr("display","block");
        $("#BNewAccessory").attr("display", "none");
    });
    $(document).on('click', '#NewAccessory-tab', function () {
        $("#BNewAds").attr("display", "none");
        $("#BNewAccessory").attr("display", "block");
    });
    $(document).on('change', '#AccessoryForAll', function () {
        var Id = $("#AccessoryForAll").val();
        if (Id === "true") {
            $(".SelectYourModels").hide();
        } else {
            $(".SelectYourModels").show();
        }
    });
    /////////////////////////////////////////////////////
    $(document).on("click", "#AddModel", function (event) {
        event.preventDefault();
        var SCat = $("#SelectedMCatId").val();
        var SBrand = $("#SelectedMBrandId").val();
        var SClass = $("#SelectedMClassId").val();
        var SModel = $("#SelectedMModelId").val();
        if (SCat !== "" && SBrand !== "" && SClass !== "" && SModel !== "") {
            var Count = 0;
            if (typeof $.cookie('SelectedModels') !== 'undefined') {
                var count = JSON.parse($.cookie("SelectedModels"));
                Count = count.length + 1;
            } else {
                Count = 1;
            }
            var row = {
                "Id": Count,
                "CatId": SCat,
                "CatName": $("#SelectedMCatId option:selected").text(),
                "BrandId": SBrand,
                "BrandName": $("#SelectedMBrandId option:selected").text(),
                "CarClassId": SClass,
                "ClassName": $("#SelectedMClassId option:selected").text(),
                "ModelId": SModel,
                "ModelName": $("#SelectedMModelId option:selected").text()
            };
            if (typeof $.cookie('SelectedModels') !== 'undefined') {
                var Exist = false;
                var Card = JSON.parse($.cookie("SelectedModels"));
                for (var i = 0; i < Card.length; i++) {
                    if (SCat === Card[i].CatId && SBrand === Card[i].BrandId && SClass === Card[i].CarClassId && SModel === Card[i].ModelId) { Exist = true; }
                }
                if (Exist === false) {
                    Card.push(row);
                    $.cookie('SelectedModels', JSON.stringify(Card), { path: '/' });
                    displaySelectedModels();
                }
            } else {
                var data = [];
                data.push(row);
                $.cookie('SelectedModels', JSON.stringify(data), { path: '/' });
                displaySelectedModels();
            }
        }
        else {
            msgError("Selected Value is Emty");
        }
    });
    function displaySelectedModels() {
        $(".YourModels").html("");
        var row = '';
        var i = 0;
        if (typeof $.cookie('SelectedModels') !== 'undefined') {
            var data = JSON.parse($.cookie("SelectedModels"));
            for (i; i < data.length; i++) {
                row += `<span><a id="${data[i].Id}" class="removeModel">X</a>${data[i].CatName} / ${data[i].BrandName} / ${data[i].ClassName} / ${data[i].ModelName}</span>`;
            }
        }
        $(".YourModels").append(row);
    }
    $(document).on("click", ".removeModel", function (event) {
        var id = parseInt($(this).attr("id"));
        var data = JSON.parse($.cookie("SelectedModels"));
        RemoveNode("Id", id, data);
    });
    function RemoveNode(property, num, arr) {
        for (var i in arr) {
            if (arr[i][property] === num)
                arr.splice(i, 1);
        }
        $.cookie('SelectedModels', JSON.stringify(arr), { path: '/' });
        displaySelectedModels();
    }
    //////////////////////////////////////////////////
    //crop config
    $image_crop = $("#image_viewer").croppie({
        enbleExif: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400
        }
    });
    //crop uplode file
    $('#ImgFile').change('change', function () {
        const file = this.files[0];
        const fileType = file['type'];
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        if (validImageTypes.includes(fileType)) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            };
            reader.readAsDataURL(this.files[0]);
        }
        else {
            alert("Uplode Image");
        }
    });
    //////////////////////////////////////////////////
    //submit data
    $("#NewAds-form").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
                function (t) {
                    if (t.value) {
                        $("#CustomPreloader").show();
                        var imageFiles = [];
                        if (window.File && window.FileList && window.FileReader) {
                            var files = $("#ImagePordect").prop("files");
                                for (var i = 0; i < files.length; i++) {
                                    (function (file) {
                                        if (file.type.indexOf("image") == 0) {
                                            var fileReader = new FileReader();
                                            fileReader.onload = function (f) {
                                                $("![]()", {
                                                    src: f.target.result,
                                                    width: 200,
                                                    height: 200,
                                                    title: file.name
                                                }).appendTo("#preview");
                                                imageFiles.push(f.target.result);
                                            };
                                            fileReader.readAsDataURL(file);
                                        }
                                    })(files[i]);
                                }
                        }
                        var data = {
                            "CatId": $("#CatId").val(),
                            "BrandId": $("#BrandId").val(),
                            "CarClassId": $("#CarClassId").val(),
                            "ModelId": $("#ModelId").val(),

                            "AdPrice": $("#AdPrice").val(),
                            "isNew": $("#isNew").val(),
                            "colorId": $("#colorId").val(),
                            "CEngineCapacityCC": $("#CEngineCapacityCC").val(),
                            "CMileageKM": $("#CMileageKM").val(),
                            "DescribeAR": $("#DescribeAR").val(),
                            "DescribeEN": $("#DescribeEN").val(),
                            "Features": $("#Features").val(),
                            "ImagePordect": imageFiles
                        };
                        POSTAuth('POST', Router.CustomerProfile.POSTNewAds, token, data, 'CreateNewAds');
                    }
                    else {
                        Cancelled();
                    }
                });
    });
    //submit data
    $("#NewAccessory-form").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
                function (t) {
                    if (t.value) {
                        $("#CustomPreloader").show();
                        var AccimageFiles = [];
                        if (window.File && window.FileList && window.FileReader) {
                            var files = $("#AccImagePordect").prop("files");
                            for (var i = 0; i < files.length; i++) {
                                (function (file) {
                                    if (file.type.indexOf("image") == 0) {
                                        var fileReader = new FileReader();
                                        fileReader.onload = function (f) {
                                            $("![]()", {
                                                src: f.target.result,
                                                width: 200,
                                                height: 200,
                                                title: file.name
                                            }).appendTo("#preview");
                                            AccimageFiles.push(f.target.result);
                                        };
                                        fileReader.readAsDataURL(file);
                                    }
                                })(files[i]);
                            }
                        }
                        var data = {
                            "CatId": $("#AccCatId").val(),
                            "MainAccessoriesType": $("#MainAccessoriesType").val(),
                            "SubAccessoriesType": $("#SubAccessoriesType").val(),
                            "AccessoryTitleEN": $("#AccessoryTitleEN").val(),
                            "AccessoryTitleAR": $("#AccessoryTitleAR").val(),
                            "Price": $("#AccessoryPrice").val(),
                            "DescribeAR": $("#AccDescribeAR").val(),
                            "DescribeEN": $("#AccDescribeEN").val(),
                            "isNew": $("#AccisNew").val(),
                            "ImagePordect": AccimageFiles,
                            "AccessoryForAll": $("#AccessoryForAll").val(),
                            "SupportDetails": JSON.parse($.cookie("SelectedModels"))
                        };
                        POSTAuth('POST', Router.CustomerProfile.POSTSellAccessory, token, data, 'CreateNewAds');
                    }
                    else {
                        Cancelled();
                    }
                });
    });

    //submit Rant Car
    $("#NewRantCar-form").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Register.title,
                text: Resource.Register.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Register.confirmButtonText,
                cancelButtonText: Resource.Register.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
            function (t) {
                    if (t.value) {
                        $image_crop.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(
                            function (response) {
                                $("#CustomPreloader").show();
                                let img_url;
                                if ($('#ImgFile').val() === "") {
                                    img_url = "";
                                } else {
                                    img_url = response;
                                }
                                var data = {
                                    "CatId": $("#RantCatDrop").val(),
                                    "BrandId": $("#RantPrandDrop").val(),
                                    "CarClassId": $("#RantClassDrop").val(),
                                    "ModelId": $("#RantModelDrop").val(),
                                    Image: img_url
                                };
                                POSTAuth('POST', Router.CustomerProfile.POSTRentsCarAds, token, data, 'ResponceFromServer');
                            });
                    }
                    else {
                        Cancelled();
                    }
                });
    });

});