﻿jQuery(document).ready(function (i) {
    $("#LoginBy").val("mail");
    $('input:radio[name="ByEmail"]').change(
        function () {
            if (this.checked) {
                $("#EmailBox").show();
                $("#PhoneBox").hide();
                $("#ByPhone").prop('checked', false);
                $("#LoginBy").val("mail");
            }
        });
    $('input:radio[name="ByPhone"]').change(
        function () {
            if (this.checked) {
                $("#EmailBox").hide();
                $("#PhoneBox").show();
                $("#ByEmail").prop('checked', false);
                $("#LoginBy").val("phone");
            }
        });

    //submit data
    $("#form-login").submit(function (event) {
        event.preventDefault();
        Swal.fire(
            {
                title: Resource.Login.title,
                text: Resource.Login.text,
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: Resource.Login.confirmButtonText,
                cancelButtonText: Resource.Login.cancelButtonText,
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2", buttonsStyling: !1
            }).then(
                function (t) {
                    if (t.value) {
                        $("#CustomPreloader").show();
                        var data;
                        var logby = $("#LoginBy").val();
                        if (logby === "phone") {
                            data = {
                                "Email": null,
                                "PhoneNumber": $("#phone").val(),
                                "Password": null

                            };
                            POST('POST', Router.Account.Login, data, 'Login');
                        } else if (logby === "mail") {
                            data = {
                                "Email": $("#email").val(),
                                "PhoneNumber": null

                            };
                            POST('POST', Router.Account.Login, data, 'Login');
                        }
                    }
                    else {
                        Cancelled();
                    }
                });
    });
});