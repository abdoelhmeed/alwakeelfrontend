﻿$(document).ready(function () {
    var data;
    let CatVal, BrandVal, ClassVal, ModelVal;
    let _Skep = 0;
    let _Take = 12;
    CategoriesFilter();
    GetAccessoresslist();
    ProductsList(null, false, false);

    //
    $(document).on('click', '.GetCat', function () {
        var box = $(this);
        if (box.is(":checked")) {
            var CatId = box.val();
            var elmId = box.attr("id");
            CatVal = CatId;

            $('.GetCat').prop("checked", false);
            $("#" + elmId).prop("checked", true);

            $("#SearchKey").val(true);
            $("#lastLentgh").val(0);

            $("#ProductsLoder").show();
            $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#ProductList-data,#ProductListView-data").hide();

            data = {
                "CatId": CatId, "BrandId": 0,
                "ClassId": 0, "ModelId": 0,
                "Skep": 0, "Take": _Take
            };
            SearchForProducts(data, true,false);
            Get('GET', Router.Shared.Brand, { CatId: CatId }, 'BrandFilter');
        }
    });
    //
    $(document).on('change', '#PrandList', function () {
        var id = $("#PrandList").val();
        BrandVal = id;
        if (id !== "") {
            $("#ProductsLoder").show();
            $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#ProductList-data,#ProductListView-data").hide();

            data = {
                "CatId": CatVal, "BrandId": id,
                "ClassId": 0, "ModelId": 0,
                "Skep": 0, "Take": _Take
            };
            SearchForProducts(data, true, false);
            Get('GET', Router.Shared.Class, { BrandId: id }, 'ClassFilter');
        }
    });
    //
    $(document).on('change', '#ClassList', function () {
        var id = $("#ClassList").val();
        ClassVal = id;
        if (id !== "") {
            $("#ProductsLoder").show();
            $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#ProductList-data,#ProductListView-data").hide();
            data = {
                "CatId": CatVal, "BrandId": BrandVal,
                "ClassId": id, "ModelId": 0,
                "Skep": 0, "Take": _Take
            };
            SearchForProducts(data, true, false);
            Get('GET', Router.Shared.Model, { CarClassId: id }, 'ModelFilter');
        }
    });
    //
    $(document).on('change', '#ModelList', function () {
        var id = $("#ModelList").val();
        ModelVal = id;
        if (id !== "") {
            $("#ProductsLoder").show();
            $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#ProductList-data,#ProductListView-data").hide();

            data = {
                "CatId": CatVal, "BrandId": BrandVal,
                "ClassId": ClassVal, "ModelId": id,
                "Skep": 0, "Take": _Take
            };
            SearchForProducts(data, true, false);
        }
    });

    //Price Slider
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 1000000,
        values: [0, 1000000],
        slide: function (event, ui) {
            $("#amount").val(ui.values[1] + " - " + ui.values[0]);
            $("#StartAmount").val(ui.values[0]);
            $("#EndAmount").val(ui.values[1]);
            $("#ProductsLoder").show();
            $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
            $("#ProductList-data,#ProductListView-data").hide();

            data = {
                "CatId": CatVal, "BrandId": BrandVal,
                "ClassId": ClassVal, "ModelId": ModelVal,
                price: ui.values[1],
                "Skep": 0, "Take": _Take
            };
            SearchForProducts(data, true, false);
        }
    });
    //
    $("#amount").val($("#slider-range").slider("values", 1) + " - " + $("#slider-range").slider("values", 0));
    //
    $(document).on('click', '#IsNew', function () {
        var checkVal;
        var box = $(this);
        if (box.is(":checked")) {
            checkVal = box.val();
        } else {
            checkVal = box.val();
        }
        var New = checkVal === "on" ? true : false;
        $("#ProductsLoder").show();
        $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        $("#ProductList-data,#ProductListView-data").hide();
        data = {
            "CatId": CatVal, "BrandId": BrandVal,
            "ClassId": ClassVal, "ModelId": ModelVal,
             ISNew: New, "Skep": 0, "Take": _Take
        };
        SearchForProducts(data, true, false);
    });
    //
    $(document).on('click', '#shop-cate-toggle li.has-sub a', function () {
        $(this).removeAttr('href');
        var element = $(this).parent('li');
        if (element.hasClass('open')) {
            element.removeClass('open');
            element.find('li').removeClass('open');
            element.find('ul').slideUp();
        } else {
            element.addClass('open');
            element.children('ul').slideDown();
            element.siblings('li').children('ul').slideUp();
            element.siblings('li').removeClass('open');
            element.siblings('li').find('li').removeClass('open');
            element.siblings('li').find('ul').slideUp();
        }
    });
    //
    $(document).on('click', '.subAccessorie', function () {
        var box = $(this).attr("id");
        $("#ProductsLoder").show();
        $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        $("#ProductList-data,#ProductListView-data").hide();
        data = {
            "CatId": 0, "BrandId": 0,
            "ClassId": 0, "ModelId": 0,
            SubAccessoriesId: box, "Skep": 0, "Take": _Take
        };
        SearchForProducts(data, true, false);
    });
    //
    async function CategoriesFilter() {
        Get("GET", Router.Shared.Categories, null, "CategoriesList");
    }
    //Get All Accessoress
    async function GetAccessoresslist() {
        Get("GET", Router.Pordect.GetAccessory, null, "Accessoresslist");
    }
    //Get All Pro
    function ProductsList() {
        var qsparam = new Array(10);
        var query = window.location.search.substring(1);
        var parms = query.split('&');
        for (var i = 0; i < parms.length; i++) {
            var pos = parms[i].indexOf('=');
            if (pos > 0) {
                var key = parms[i].substring(0, pos);
                var val = parms[i].substring(pos + 1);
                qsparam[i] = val;
            }
        }

        if (qsparam[0] === "company") {
            data = {
                "CatId": 0, "BrandId": qsparam[2],
                "BId": qsparam[1], "Skep": 0, "Take": _Take
            };
        }
        else if (qsparam[0] === "searchBar") {
            data = {
                "CatId": qsparam[1], "BrandId": qsparam[2],
                "ClassId": qsparam[3], "ModelId": qsparam[4],
                "Skep": 0, "Take": _Take
            };
        }
        SearchForProducts(data, false, false);

        $("#ProductsLoder").show();
        $("#ProductsLoder").html(`<img class="full_screen" src="/Assets/img/Scal/BlogsLoder.gif" />`);
        $("#ProductList-data,#ProductListView-data").hide();
    }
    //Get All Pro By
    function SearchForProducts(data, Search = null, LodeMore = null) {
        if (LodeMore === true) {
            $('.load_more_animation').show();
        } else {
            $('.load_more_animation').hide();
        }
        setTimeout(function () {
            Get('GET', Router.Pordect.GetPordect, data, 'ProductView', Search, LodeMore);
        }, 1000);
    }

    //////////////////////////////////////////////////////
    $(document).on("click", "#load-more", function (event) {
        event.preventDefault();
        let startIndex = parseInt($("#lastLentgh").val());
        data.Skep = startIndex;
        var SKey = $("#SearchKey").val();

        SearchForProducts(data, SKey, true);
    });

    $(document).on("click", ".LikeBtn", function () {
        var Id = $(this).attr("id");
        Get('GET', Router.Home.GetSetlove, { Id: Id }, 'Like', Id);
    });
});