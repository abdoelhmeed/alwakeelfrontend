﻿//
let row = '';
let row1 = '';
let lang = '';
let groupCounter = 0;
let i, j, x;
let today;
let D;
let M;
let priceConvertO;
//Get Status
let Status = {
    "_True": true,
    "_False": false
};

let CompanesType = {
    "AgentOil": "1",
    "AgentBattery": "2",
    "AgentTires": "3",
    "Rental": "4",
    "Insurance": "6"
};

//time To Seconds
async function timeToSeconds(time) {
    time = time.split(/:/);
    return time[0] * 3600 + time[1] * 60 + time[2];
}

//Get Pram in url
var getUrlParameter = async function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function convertdate(date) {
    var _date = new Date(date);
    var D = String(_date.getDate()).padStart(2, '0');
    var M = String(_date.getMonth() + 1).padStart(2, '0');
    var Y = _date.getFullYear();
    return D + '-' + M + '-' + Y;
}

async function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
async function ConvertO(data) {
    var result;
    if (data === 0) {
        result = "---";

    } else {
        result = data;
    }
    return result;
}