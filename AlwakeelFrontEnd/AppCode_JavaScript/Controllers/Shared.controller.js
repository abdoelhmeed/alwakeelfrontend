﻿$(document).ready(function () {
    HeaderAds();
    CategoriesFilter();
    MenuOurPartners();
    async function HeaderAds() {
            Get("GET", Router.Home.GetHeaderAds, null, "GetHeaderAds");
    }
    async function CategoriesFilter() {
            Get("GET", Router.Shared.Categories, null, "CategoriesFilter");
    }
    $(document).on('change', '#CatDrop', function () {
        var id = $("#CatDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Brand, { CatId: id }, 'BrandFilter');
        }
        else {
            $("#PrandDrop").html(`<option value=''>${Resource.Sheard.SelectCat}</option>`);
            //msgError("select section first");
        }
    });
    $(document).on('change', '#PrandDrop', function () {
        var id = $("#PrandDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Class, { BrandId: id }, 'ClassFilter');
        }
        else {
            $("#ClassDrop").html("<option value=''>Select</option>");
            //msgError("select section first");
        }
    });
    $(document).on('change', '#ClassDrop', function () {
        var id = $("#PrandDrop").val();
        if (id !== "") {
            Get('GET', Router.Shared.Model, { CarClassId: id }, 'ModelFilter');
        }
        else {
            $("#ClassDrop").html("<option value=''>Select</option>");
            //msgError("select section first");
        }
    });
    $("#searchBar").submit(function (event) {
        event.preventDefault();
        var Cat = $("#CatDrop").val();
        var Prand = $("#PrandDrop").val();
        var Class = $("#ClassDrop").val();
        var Model = $("#ModelDrop").val();
        //var GoRentCar = $("#GoRentCar").val();
        if (Cat !== "") {
            //if (GoRentCar === "1") {
            //    window.location = '/RentCars/List?filter=searchBar&CatId=' + Cat + '&BrandId=' + Prand + '&ClassId=' + Class + '&ModelId=' + Model;
            //} else {
                window.location = '/Products/List?filter=searchBar&CatId=' + Cat + '&BrandId=' + Prand + '&ClassId=' + Class + '&ModelId=' + Model;
            //}
        }
    });

    $("#PupSubscribe-form").submit(function (event) {
        event.preventDefault();
        var PupSubscribeEmail = $("#PupSubscribeEmail").val();
        if (PupSubscribeEmail !== "") {
            POST('POST', Router.Shared.CustomerSubscribe, { Email: PupSubscribeEmail }, 'ResponceFromServer');
        } else {
            msgError(Resource.Sheard.EnterEmail);
        }
    });

    $("#subscribe").submit(function (event) {
        event.preventDefault();
        var subscribeEmail = $("#subscribeEmail").val();
        if (subscribeEmail !== "") {
            POST('POST', Router.Shared.CustomerSubscribe, { Email: subscribeEmail }, 'ResponceFromServer');
        } else {
            msgError(Resource.Sheard.EnterEmail);
        }
    });
    async function MenuOurPartners() {
            //Get("GET", Router.Home.OurPartners, null, "OurPartners");
            HtmlMenuOurPartners(SiteResource.OurPartners);
    }
});
