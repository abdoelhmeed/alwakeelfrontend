﻿//English Resource File
const Resource = {
    "HTTP": {
        "": "",
        "": "",
        "": "",
        "": ""
    },
    "Base": {
        "Labeltitle": "آسف",
        "LabelgodTitle": "عمل جيد!",
        "LabelcanTitle": "ألغيت",
        "Labeltext": "بياناتك آمنة :)"
    },
    "Sheard": {
        "SelectCat": "اختار تصنيف",
        "EnterEmail": "ادخال البريد الخاص بك",
        "SelectModel": "اختر الموديل"
    },
    "Register": {
        "title":"هل أنت واثق؟",
        "text":"لن تتمكن من التراجع عن هذا!",
        "confirmButtonText":"نعم ، اريد تسجيل حساب جديد",
        "cancelButtonText": "لا ، ألغي!"
    },
    "Contact": {
        "confirmButtonText": "نعم ،  اريد  إرسال الرسالة",
        "cancelButtonText": "لا ، ألغي!"
    },
    "Login": {
        "title": "هل أنت واثق؟",
        "text": "لن تتمكن من التراجع عن هذا!",
        "confirmButtonText": "نعم اريد تسجيل دخول لحسابي",
        "cancelButtonText": "لا ، ألغي!"
    },
    "Home": {
        "SDG": "ج.س",
        "Certified": "معتمد",
        "AddToWishListBtn":"اعجبني",
        "AddToCartBtn": "أضف إلى السلة",
        "Featured": "متميز",
        "LatestVideos": "أحدث الفيديو",
        "Accessories&AutoParts":"اكسسوارات وقطع غيار سيارات",
        "ReadMore": "قراءة المزيد"
    },
    "Company": {
        "Brand": "الماركات",
        "SelectCompany": "حدد الشركة أولاً",
        "Contact_Social": "الاتصال ووسائل التواصل الاجتماعي"

    },
    "RentCar": {
        "BtnRequest": "طلب تأجير مركبة",
        "SelectCar": " حدد السيارة أولاً"
    },
    "PorductDetails": {
        "Color": "لون :",
        "EngineCapacity": "سعة المحرك",
        "MileageKM": "عدد الكيلومترات",
        "Contacts": "جهات الاتصال",
        "Views": "المشاهدات",
        "Love": "الاعجاب",
        "FeatureDetails": "تفاصيل ميزة"
    }
};
