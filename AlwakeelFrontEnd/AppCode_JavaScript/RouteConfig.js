﻿//'Route Config File'
var Router = {
    "BaseUrl": {
        "online": "http://abdoelhmeedsudan-001-site4.atempurl.com/",
        "local": ""
    },
    "Shared": {
        "Categories": "Filters/GetCarsCategories",
        "AccessoriesCategories": "Filters/GetAccessoriesCategory",
        "Brand": "Filters/GetBrand",
        "Class": "Filters/GetClass",
        "Model": "Filters/GetModel",
        "CustomerSubscribe": "subscribe/CustomerSubscribe",
        "Colors": "Filters/GetColors",
        "GetFeatures": "Filters/GetFeatures"
    },
    "Account": {
        "Register": "Auth/Register",
        "Login": "Auth/Login",
        "Verification": "Auth/Verification"
    },
    "Home": {
        "Slider": "HomePage/GetSlider",
        "GetCompanies":"HomePage/GetCompaniesByCatId",
        "OurPartners": "HomePage/GetOurPartners",
        "GetBlog": "HomePage/GetBlog",
        "GetDealOfday": "HomePage/GetDealOfday",
        "YouTubeVideos": "HomePage/GetYouTubeVideos",
        "GetBrand": "SearchVehicles/GetBrand",
        "GetFeatured":"HomePage/GetFeatured",
        "GetCertificatesd": "HomePage/GetCertificates",
        "GetAccessory": "HomePage/GetAccessory",
        "GetHeaderAds":"HomePage/GetHeaderAds",
        "GetMiddleAds":"HomePage/GetMiddleAds",
        "GetFooterAds": "HomePage/GetFooterAds",
        "GetAboutAlwakeel":"AboutAlwakeel/About",
        "PostContactAlwakeel": "AboutAlwakeel/Contact",
        "GetSetlove": "HomePage/Setlove"
    },
    "Companies": {
        "GetAgentOil": "AgentCompany/GetAgentOil",
        "GetAgentBattery": "AgentCompany/GetAgentBattery",
        "GetAgentRent": "AgentCompany/GetRentCarCompanys",
        "GetAgentSpareparts": "AgentCompany/GetAgentSpareparts",
        "GetAgentTires": "AgentCompany/GetAgentTires",
        "GetInsurance": "AgentCompany/GetInsurance",
        "GetInsuranceById": "AgentCompany/GetCompanyById",
        "PostNewBayNow":"AgentCompany/NewBayNow"
    },
    "RentCar": {
        "GetRentCarSearch":"RentCar/ShearhCars",
        "GetRentCars": "RentCar/GetRentCars",
        "PostRentCar": "RentCar/OrderCars"
    },
    "BlogPage":
    {
        "GetBlog":"Blog/GetBlog"
    },
    "Pordect": {
        "":"AdsProduct/GetColors",
        "GetAccessory": "Filters/GetMainWithSubAccessory",
        "GetPordect": "Products/GetPordectList",
        "GetPorductDetails": "Products/GetPorduct"
    },
    "CustomerProfile": {
        "GetMyProducts": "CustomerProfile/MyProducts",
        "GetMyRentCars": "CustomerProfile/MyRentCars",
        "POSTRentsCarAds": "CustomerProfile/RentsCarAds",
        "POSTNewAds": "SellPorducts/SellCras",
        "POSTSellAccessory": "SellPorducts/Sellaccessory",
        "GetAccessoriesCategories":"SellPorducts/GetAccessoriesCategories",
        "GetMainAccessoriesType":"SellPorducts/GetMainAccessoriesType",
        "GetSubAccessoriesType": "SellPorducts/GetSubAccessoriesType"
    }
};
//Base Url
let Url = Router.BaseUrl.online + "api/";
